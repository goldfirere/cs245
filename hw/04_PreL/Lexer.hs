{- Author: <your name here>
   File: Lexer.hs

   Lexes the syntax for the Preλ interpreter
-}

module Lexer where

import Data.Char
import Text.Read

import Token
import Syntax

-- Lex a Preλ expression into a list of tokens
-- Calls `error` if there is a lexical error (something that
-- doesn't lex)
lexPreL :: String -> [Token]
lexPreL = error "unimplemented"
