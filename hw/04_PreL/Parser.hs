{- Author: <your name here>
   File: Parser.hs

   Parses the Preλ syntax
-}

module Parser where

import Token
import Syntax

-- Parse an expression, returning the parsed expression
-- and a list of unconsumed tokens
-- Calls `error` if the list of tokens has no valid parse.
parse :: [Token] -> (Expr, [Token])
parse = error "unimplemented"
