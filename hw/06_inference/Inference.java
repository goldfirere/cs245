/* Name: <your name here>
   File: Inference.java

   Write the most general type for each of the following methods. Note that
   a most general type makes the fewest assumptions about its inputs (parameters)
   and offers the most information about its outputs (return type). That means
   that ArrayList<String> frob(List<String> l) is more general than
   ArrayList<String> frob(ArrayList<String> l), which is more general than
   List<String> frob(ArrayList<String> l).

   Include tests for your functions in the main method. Note that this assignment
   is all about *compiling*, not about runtime behavior (though your methods shouldn't
   fail at runtime either).
*/

import java.util.*;

public class Inference
{
  // Swap the two elements in an HPair
  public static ... hpairSwap(... hpair)
  {
    ... temp = hpair.getFirst();
    hpair.setFirst(hpair.getSecond());
    hpair.setSecond(temp);
  }

  // Copy the elements of an HPair into a Pair
  public static ... setPair(... pair, ... hpair)
  {
    pair.setFirst(hpair.getFirst());
    pair.setSecond(hpair.getSecond());
  }

  // Copy the contents of a Pair into an HPair
  public static ... setHPair(... hpair, ... pair)
  {
    hpair.setFirst(pair.getFirst());
    hpair.setSecond(pair.getSecond());
  }

  // Given a map m, add a new binding in m for each value v in m, where v maps to itself
  public static ... selfMap(... m)
  {
    for(... v : new ArrayList<>(m.values()))
    {
      m.put(v,v);
    }
  }

  // Add all the elements in a collection of collections into a target data structure.
  public static ... unions(... to, ... from)
  {
    for(... coll : from)
    {
      for(... e : coll)
      {
	to.add(e);
      }
    }
  }

  // Sort a list in place
  public static ... sort(... list)
  {
    for(int i = 0; i < list.size() - 1; i++)
    {
      ... minIndex = i;
      for(int j = i+1; j < list.size(); j++)
      {
	if(list.get(minIndex).compareTo(list.get(j)) > 0)
	{
	  minIndex = j;
	}
      }

      ... temp = list.get(minIndex);
      list.set(minIndex, list.get(i));
      list.set(i, temp);
    }
  }

  // Find the maximum element in a collection of collections; returns null
  // if there are no elements in the collections
  public static ... maxs(... colls)
  {
    ... max = null;
    for(... coll : colls)
    {
      for(... elem : coll)
      {
	if(max != null)
	{
	  if(max.compareTo(elem) < 0)
	  {
	    max = elem;
	  }
	}
	else
	{
	  max = elem;
	}
      }
    }
    return max;
  }

  // Given a collection from, copy all the elements from 'from' into several "to" collections
  public static ... copyTo(... from, ... tos)
  {
    for(... coll : tos)
    {
      for(... elem : from)
      {
	coll.add(elem);
      }
    }
  }

  // Reverse a map; that is, produce a new map where each value in the original maps to its
  // key. If a value in the original is mapped to by several keys, the resulting map will
  // map that value to one such key, chosen arbitrarily.
  public static ... reverseMap(... m)
  {
    ... result = new ComparableTreeMap<>();
    for(... k : m.keySet())
    {
      result.put(m.get(k), k);
    }
    return result;
  }

  // Like reverseMap, but if a value is mapped by multiple keys, stores all such keys
  // in a set.
  public static ... reverseMapMany(... m)
  {
    ... result = new HashMap<>();
    for(... k : m.keySet())
    {
      ... v = m.get(k);
      if(!result.containsKey(v))
      {
	result.put(v, new HashSet<>());
      }
      result.get(v).add(k);
    }
    return result;
  }

  // Extract the keys and values from a map.
  public static ... keysAndValues(... m)
  {
    return new HPair<>(m.keySet(), m.values());
  }

  // Given two lists, produce a list of Pairs of the corresponding elements of the lists.
  public static ... zip(... as, ... bs)
  {
    ... result = new ArrayList<>();
    for(int i = 0; i < as.size() && i < bs.size(); i++)
    {
      result.add(new Pair<>(as.get(i), bs.get(i)));
    }
    return result;
  }

  // Given a list of Pairs, produce a Pair of lists containing the original elements
  public static ... unzip(... asbs)
  {
    ... as = new ArrayList<>();
    ... bs = new ArrayList<>();
    for(... pair : asbs)
    {
      as.add(pair.getFirst());
      bs.add(pair.getSecond());
    }
    return new Pair<>(as, bs);
  }

  // Given two lists, produce a list of HPairs of the corresponding elements of the lists.
  public static ... hzip(... as, ... bs)
  {
    ... result = new ArrayList<>();
    for(int i = 0; i < as.size() && i < bs.size(); i++)
    {
      result.add(new HPair<>(as.get(i), bs.get(i)));
    }
    return result;
  }

  // Given a list of HPairs, produce an HPair of lists containing the original elements.
  public static ... hunzip(... asbs)
  {
    ... as = new ArrayList<>();
    ... bs = new ArrayList<>();
    for(... pair : asbs)
    {
      as.add(pair.getFirst());
      bs.add(pair.getSecond());
    }
    return new HPair<>(as, bs);
  }

  // Given a collection of pairs, return the String representation of the greater element
  // of the pair, according to compareTo.
  // It is an interesting puzzle as to why this must return string representations, not
  // the elements themselves. It is not required as part of this homework, but it may be
  // interesting to try to answer this question.
  public static ... maxes(... coll)
  {
    ... result = new ArrayList<>();
    for(... pair : coll)
    {
      if(pair.getFirst().compareTo(pair.getSecond()) > 0)
      {
	result.add(pair.getFirst().toString());
      }
      else
      {
	result.add(pair.getSecond().toString());
      }
    }
    return result;
  }

  public static void main(String[] args)
  {
    // put any tests here
  }
}
