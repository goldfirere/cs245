---
title: CS 245 Policies
---

<div id="header">

| **CS 245 Course Policies**
| Prof. Richard Eisenberg
| Fall 2018

</div>

\$navbar\$

Prerequisites
=============

All students should have completed the introductory computer science sequence,
including some introductory course (B110, B113, H105, H107, or BIO B115),
some data structures course (B206, H106, or H107), and discrete mathematics
(B/H231). Alternatively, students may enroll in CS B/H231 concurrently with
CS 245.

Workload
--------

It is my expectation that this course is difficult. The reward is that you will
learn a lot about programming languages -- these tools you will use throughout
your CS career. I expect that you will
have to spend 6-8 hours working outside of class/lab every week. For a two-week
assignment, this means that the assignment might take 12-16 hours or more.

Grading
=======

<div class="grading_table">

----------------------------  --------------
Participation                  5%
Assignments                    25%
Quizzes                        10%
Exam 1                         15%
Exam 2                         15%
Final Exam                     30%
<span class="strut" />
Total                          100%
----------------------------  --------------

</div>

Individual assingments/exams will be graded on a percentage basis. At the
end of the semester, these grades will be translated into the 4-point scale
roughly according to the following table (without rounding):

<div class="grading_table">

----------------------------  --------------
**Percentage**                **Grade**
0-50%                         0.0
50-57%                        1.0
57-63%                        1.3
63-69%                        1.7
69-74%                        2.0
74-78%                        2.3
78-82%                        2.7
82-86%                        3.0
86-90%                        3.3
90-94%                        3.7
94-100%                       4.0
----------------------------  --------------

</div>

Note that the correspondence between percentage grade and transcript grade
here is rough; we will discuss updates to this chart throughout the semester.

Assignment grading
------------------

All assignments are graded both for correctness and for style.

With every homework assignment, you will submit a transcript of running several
tests against your program. The tests reported in these transcripts will be
student-sourced: on [Piazza], I will post a few tests for each part of an
assignment. Then, you take over, adding more tests that cover more and more
corner cases. Contributing to these tests is one way to improve your participation
grade. (The horizontal scroll bar at the top of posts allow anyone to see a post's
history and who added/removed pieces.)

Your correctness grade is *not* simply based on a ratio of succeeding tests
to all tests, but instead the performance on these tests help clue in the
graders (which will include the TAs) to what might be wrong with your code.
If you pass all the tests (and enough corner cases are covered in the tests), then
you'll get a full score on correctness.

Code that does not compile will get a 0 on correctness.

Style grading assesses how well you've conformed to the [Style
Guide](style.html), which dictates how you should write your code. Programming
is an act of communication both between you and a computer *and* between you and
another human. In some sense, correctness is about the former,
while style is about the latter.

We will take a collaborative approach toward style assessment. After you submit
your homework for correctness grading, you will meet with two randomly-assigned
teammates. As a group, you will work together to revise and combine your assignments
to produce one combined assignment which will be graded for style. This is also
a time where you can learn from your peers about correctness issues, too.
You will then submit your combined work which will be graded for style.
In addition, for some of the assignments, your group will meet with me to
discuss the assignment. This is a required part of the course, factored into
the participation grade.

Your submitted work is a reflection of your progress in this course and your
level of attention to detail as a student. You should always strive to submit
your best work, free of messy formatting, spelling errors, and careless errors.
The programs you write will often interact with the user; you should strive to
make the user's experience pleasant and your programs easy to use.

Assignments will be returned (via [Gradescope]) with comments. Please read these
comments, and feel free to ask any questions if you need further clarification.

Class Participation
-------------------

This component of your grade is a reflection of how you have contributed to
this class. It includes speaking up in class, attendance, and engagement. I expect
every student to contribute to the class environment, both to improve your own
experience and to improve the experience of others. For example, you can be an
active partner when working in groups, you can post on
[Piazza], you can raise your hand
in class, and you can visit my [office hours](index.html) -- but there are
other ways to contribute, as well.

A fantastic way to contribute is to find ways to improve this material. Submit
a [merge request](https://gitlab.com/goldfirere/cs245) against the `cs245` repo!

Quizzes
-------

There will be occasional quizzes during labs. While we will not have during
every lab, you should come to lab expecting a quiz; they will not generally
be announced. The first of these will be the second week of class.

Exams
-----

This course has two midterms (Oct. 25 and Nov. 20), and one self-scheduled
final exam during the normal exam period.
Exams will be open-book and open-note (not open-compiler). Students
using an electronic version of the book or taking electronic notes
may consult those resources: you can do so only with a laptop (no
tablets/phones), with its network connection disabled, and with no use
of the keyboard.

If you miss an exam without emailing me first, you will get a 0 on that
exam. Changes to the exam schedule will be considered only in extreme
circumstances.

Late policy
===========

Assignments will generally be due Tuesday nights at midnight.
You will submit assignments via
[Gradescope], as practiced during [Lab 1](lab01/lab.html).

Late assignments will lose 20% of their points for every day
late (or portion thereof). Each student gets 4 free *late days* for the semester.
This means that the first four days (or portion thereof) that an assignment
is late will not lead to a penalty. Late days cannot be split; in other words,
the count of the late das available is always an integer.
These late days are intended to account for
unexpected bugs, minor illnesses, planned travel, etc. I will grant further
extensions only in rare circumstances. You do not have to request to use a late day;
they are automatically granted.

Group work policy
=================

Working with a partner
----------------------

You are allowed to work with up to one partner on all assignments.

If you work with a partner:

 * Make sure that both partners' names are on all parts of the assignment.
 * Register as a group within [Gradescope].
 * Submit only one copy of the assignment.

Two partners working together receive one shared grade. You may *not* collaborate
on one part of an assignment and then work separately on other parts.

Partners will generally be split up when you are divided into style groups; each
partner will adapt the code independently into the version combined with the
others in the style group.

Collaboration policy
--------------------

(In this section, if you are working with a partner, you and your partner are
considered "you". It is expected that partners share code!)

You are encouraged to brainstorm with others for assignments, **but your submission
must be your own**:

<div id="plagiarism">
All the code you submit must be written by you alone.
</div>

This means that, while it's a great idea to discuss general algorithms or
approaches with your classmates, you should make sure that the code you
submit is yours and yours alone. This means that it may be OK to help a
friend debug their code or to look at someone else's approach for inspiration,
but **your own writeup must be made independently,** without the other code
available for consultation.

If your work has been influenced by the work of another student, **you must
cite that student** in your submission. (This citation can take the form
of a comment in that area of your code; there is no prescribed citation
format.) Note that a citation is necessary even if it's just an influence.
These citations have no negative impact on your grade.

Submitting code from any source other than your own brain without citation
is a violation of Bryn Mawr's Honor Code. In particular, **never submit code
you have found online**.

If you have a question, post on [Piazza].

Piazza
======

There is a course question-and-answer forum at
<https://piazza.com/brynmawr/fall2018/cs245/home>. This forum is hosted by an
educational service, Piazza, and it will be the primary means of electronic
communication in this course. It is free to use. (Piazza's business model is about
connecting students with job recruiters, a feature called Piazza Careers.
Recruiters pay for this connection. Your questions and other class participation
remain strictly private. During
sign-up, you may choose whether you wish to participate in Piazza Careers.)
Piazza offers the ability to ask
questions anonymously (either anonymous to everyone or only to fellow students, but not
instructors) and for one student to see others' questions. You may also use the
site to send private messages/questions to the instructors of the course.

If you have a question about the material, the course, extensions, or anything else,
please use Piazza.

Piazza also has the ability to send out announcements. Any coursewide information will
go through that site, which will send everyone an email with the info.

I do tend to respond to questions during business hours only; something posted Friday
evening might not get a response until Monday. However, an advantage to posting on
Piazza is that a fellow student or a TA might answer a question before I can.

GitLab
======

The course materials are hosted on [GitLab](https://gitlab.com/goldfirere/cs245).
Feel free to submit corrections/ask clarifications there, via GitLab's [merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)
or [issues](https://docs.gitlab.com/ee/user/project/issues/) mechanisms.

Accommodations for disability
=============================

Bryn Mawr College is committed to providing equal access to students with a
documented disability. Students needing academic accommodations for a
disability must first register with Access Services. Students can call
610-526-7516 to make an appointment with the Coordinator of Access Services,
Deb Alder, or [email her](mailto:dalder@brynmawr.edu) at `dalder@brynmawr.edu`
to begin this confidential process. Once registered, students should schedule
an appointment with me as early in the semester as possible to share the
verification form and make appropriate arrangements. Please note that
accommodations are not retroactive and require advance notice to implement.
More information can be obtained at the [Access Services
website](http://www.brynmawr.edu/access_services/).

Meetings
========

My office hours for Fall 2018 are Tuesdays 11:25-12:25 and Wednesdays 1:30-2:30.
This means that, at
these hours, I am guaranteed to be in my office and expecting visitors -- and
I really do want visitors. During class, it's hard to get to know all of you,
and I'd love to know more about what brought you into my class, what else
you're interested in (in computer science and more broadly), and how your
college experience is going generally. Come with a question, come to say hi,
or come to play one of my puzzles. You can even use your curiosity about my
puzzle collection as an excuse to get in the door.

If you have a conflict with my office hours, please email so we can find another
time to meet.

Beyond my office hours, I aim to have an open-door policy. If you see my
office door (Park 204) open, please interrupt me: that's why the door is
open!

For a broader discussion than just homework questions, I'd be happy to join
you for lunch in the dining hall. Just email or ask!

[Piazza]: https://piazza.com/brynmawr/fall2018/cs245/home
[Gradescope]: https://gradescope.com/
