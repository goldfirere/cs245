import java.util.*;

public class Example
{
  public static void main(String[] args)
  {
    ArrayList<Integer> ints = new ArrayList<>();
    ArrayList objs = ints;
    ArrayList<String> strs = objs;

    strs.add("hello");
    System.out.println(ints.get(0).intValue());
  }

  public static <T> void addAll(Collection<T> to, List<? extends T> from)
  {
    for(int i = 0; i < from.size(); i++)
    {
      to.add(from.get(i));
    }
  }
}
