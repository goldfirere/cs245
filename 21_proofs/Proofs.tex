\documentclass{article}

\usepackage[T1]{fontenc}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{hyperref}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}

\begin{document}

\begin{center}
\bf
CMSC 245: Principles of Programming Languages \\
Type System Proofs
\end{center}

This document was prepared using \LaTeX, a programming language designed
to typeset mathematical writing. If you do not yet know how to write \LaTeX,
I strongly encourage you to learn. Many resources can be found online.

Here is the definition of the simply-typed $\lambda$-calculus (STLC):

\newcommand{\bnfeq}{{:}{:}{=}}
\newcommand{\bnfor}{\mathrel{|}}
\newcommand{\difrac}[2]{\frac{\displaystyle #1}{\displaystyle #2}}
\newcommand{\steps}{\longrightarrow}
\newcommand{\Int}{\textbf{Int}}
%
\begin{gather*}
\begin{array}{ll}
\begin{array}{rcl}
e &\bnfeq& x \bnfor v \bnfor e_1\,e_2 \bnfor e_1 + e_2 \\
v &\bnfeq& \lambda x{:}\tau. e \bnfor n \\
\tau &\bnfeq& \Int \bnfor \tau_1 \to \tau_2 \\
\end{array}
&
\begin{array}{rcl}
x &\bnfeq& \text{(named variables)} \\
n &\bnfeq& \text{(integers)} \\
\Gamma &\bnfeq& \emptyset \bnfor \Gamma, x{:}\tau
\end{array}
\end{array}\\[3ex]
%
\fbox{$\displaystyle \Gamma \vdash e : \tau$}\\[2ex]
\difrac{x{:}\tau \in \Gamma}{\Gamma \vdash x : \tau}\textsc{Var}
\qquad
\difrac{\Gamma \vdash e_1 : \tau_1 \to \tau_2 \quad \Gamma \vdash e_2 : \tau_1}{\Gamma \vdash e_1\,e_2 : \tau_2}\textsc{App}
\qquad
\difrac{\Gamma,x{:}\tau_1 \vdash e : \tau_2}{\Gamma \vdash \lambda x{:}\tau_1. e : \tau_1 \to \tau_2}\textsc{Abs}
\\[2ex]
\difrac{}{\Gamma \vdash n : \Int}\textsc{Int}
\qquad
\difrac{\Gamma \vdash e_1 : \Int \quad \Gamma \vdash e_2 : \Int}{\Gamma \vdash e_1 + e_2 : \Int}\textsc{Plus}
\\[3ex]
\fbox{$\displaystyle e_1 \steps e_2$}\\[2ex]
\difrac{e_1 \steps e_1'}{e_1 \, e_2 \steps e_1' \, e_2}\textsc{App1}
\qquad
\difrac{e_2 \steps e_2'}{v_1 \, e_2 \steps v_1 \, e_2'}\textsc{App2}
\qquad
\difrac{}{(\lambda x{:}\tau. e)\,v \steps e[x \mapsto v]}\textsc{Beta}
\\[2ex]
\difrac{e_1 \steps e_1'}{e_1 + e_2 \steps e_1' + e_2}\textsc{Plus1}
\qquad
\difrac{e_2 \steps e_2'}{v_1 + e_2 \steps v_1 + e_2'}\textsc{Plus2}
\qquad
\difrac{}{n_1 + n_2 \steps \textsf{plus}(n_1,n_2)}\textsc{Add}
\end{gather*}
\begin{itemize}
\item The notation $e[x \mapsto v]$ means to replace every occurrence of $x$ in $e$ with $v$.
\item The notation $\textsf{plus}(n_1, n_2)$ indicates the sum of the numbers $n_1$ and $n_2$.
\item We assume all variables in $\Gamma$ to be pairwise distinct. Note that this can always
be upheld by the use of $\alpha$-conversion.
\item Expressions may be written with parentheses to aid in parsing; these parentheses are then
ignored in this formalism.
\end{itemize}

\begin{theorem}
For all types $\tau$, $\tau$ mentions $\Int$.
\end{theorem}

\begin{proof}
By structural induction on $\tau$.
\begin{description}
\item[Case $\tau = \Int$:] We observe that $\tau$ mentions $\Int$ and are done with this case.
\item[Case $\tau = \tau_1 \to \tau_2$:] Our induction hypothesis is that $\tau_1$ mentions $\Int$
and that $\tau_2$ mentions $\Int$. Thus $\tau$ mentions $\Int$.
\end{description}
\end{proof}

\begin{theorem}
For all expressions $e$, if $\emptyset \vdash e : \tau$ for some type $\tau$, then $e$ contains
either a $\lambda$ or an integer $n$.
\end{theorem}

\begin{proof}
By structural induction on $e$.
\begin{description}
\item[Case $e = x$:] This case cannot happen. We know that $\emptyset \vdash e : \tau$ and that
$e = x$. Thus $\emptyset \vdash x : \tau$. This can be proved only by \textsc{Var}. However,
that rule's premise requires that $x{:}\tau \in \emptyset$ (in our case), which is impossible.
\item[Case $e = e_1\,e_2$:] Our induction hypothesis states that, if $\emptyset \vdash e_1 : \tau_3$
(for some $\tau_3$), then $e_1$ contains a $\lambda$ or an integer $n$. The induction hypothesis
holds also for $e_2$. To use the induction hypothesis, we must show that $\emptyset \vdash e_1 : \tau_3$
for some $\tau_3$. We know $\emptyset \vdash e_1\,e_2 : \tau$. This can happen only by \textsc{App}.
Thus, the premises for \textsc{App} must also hold. Thus, we know $\emptyset \vdash e_1 : \tau_1 \to \tau_2$
for some $\tau_1$ and $\tau_2$. This satisfies the premise to the induction hypothesis, so we can
conclude that $e_1$ contains either a $\lambda$ or an integer $n$. Accordingly, $e_1\,e_2$ contains
a $\lambda$ or an integer $n$, and so we are done with this case.
\item[Case $e = e_1 + e_2$:] Similar to the previous case. Note that we use \textsc{Plus}, which
contains as a premise that $e_1$ and $e_2$ are well typed.
\item[Case $e = \lambda x{:}\tau_1. e_1$:] Clearly, $e$ contains a $\lambda$, so we are done.
\item[Case $e = n$:] Clearly, $e$ contains an integer $n$, so we are done.
\end{description}
\end{proof}

\begin{lemma}[Weakening]
\label{lem:weakening}
For all contexts $\Gamma$ and $\Gamma'$, expressions $e$, and types $\tau$:
if $\Gamma \vdash e : \tau$, then $\Gamma, \Gamma' \vdash e : \tau$.
\end{lemma}

A simplified version of this proof is assigned for homework.

\begin{lemma}[Substitution]
\label{lem:subst}
For all expressions $e_1$: For all contexts $\Gamma$ and $\Gamma'$, all expressions $e_2$, 
all types $\tau_1$ and $\tau_2$, and all variables $x$,
if $\Gamma, x{:}\tau_2, \Gamma' \vdash e_1 : \tau_1$
and $\Gamma \vdash e_2 : \tau_2$,
then $\Gamma, \Gamma' \vdash e_1[x \mapsto e_2] : \tau_1$.
\end{lemma}

\begin{proof}
By structural induction on $e_1$.
\begin{description}
\item[Case $e_1 = x$:] We must prove $\Gamma, \Gamma' \vdash x[x \mapsto e_2] : \tau_1$; that is,
we must prove $\Gamma, \Gamma' \vdash e_2 : \tau_1$.
We know $\Gamma, x{:}\tau_2, \Gamma' \vdash x : \tau_1$. This can be proved only by \textsc{Var}.
Thus, we must have $x{:}\tau_1 \in \Gamma, x{:}\tau_2, \Gamma'$. But by the assumption that
all elements in a context are pairwise distinct, we know that $x$ cannot occur in $\Gamma$ or
$\Gamma'$. Thus, the $x{:}\tau_2$ binding is the only binding for $x$ in $\Gamma, x{:}\tau_2, \Gamma'$.
Since we know $x{:}\tau_1 \in \Gamma,x{:}\tau_2,\Gamma'$, it must be that $\tau_1 = \tau_2$.
Thus, we can conclude that $\Gamma \vdash e_2 : \tau_1$. By the Weakening Lemma~(Lemma~\ref{lem:weakening}),
we can further conclude that $\Gamma, \Gamma' \vdash e_2 : \tau_1$, just as we wanted to show.
\item[Case $e_1 = x'$ (where $x \neq x'$):] We must prove $\Gamma, \Gamma' \vdash x'[x \mapsto e_2] : \tau_1$; that is,
we must prove $\Gamma, \Gamma' \vdash x' : \tau_1$. We have assumed
$\Gamma, x{:}\tau_2, \Gamma' \vdash x' : \tau_1$. Since $x \neq x'$, then the binding for $x'$ must be
in either $\Gamma$ or $\Gamma'$; that is, either $x'{:}\tau_1 \in \Gamma$ or $x'{:}\tau_1 \in \Gamma'$
 Accordingly, $\Gamma, \Gamma' \vdash x' : \tau_1$ as desired.
\item[Case $e_1 = e_3\,e_4$:] Our induction hypothesis tells us that,
if $\Gamma, x{:}\tau_2, \Gamma' \vdash e_3 : \tau_3$ (for some $\tau_3$), then
$\Gamma, \Gamma' \vdash e_3[x \mapsto e_2] : \tau_3$ (and similarly for $e_4$).
We have assumed $\Gamma, x{:}\tau_2, \Gamma' \vdash e_3\,e_4 : \tau_1$. This can be true
only by \textsc{App}. Thus, the premises of \textsc{App} must be true; we can conclude
that $\Gamma, x{:}\tau_2, \Gamma' \vdash e_3 : \tau_4 \to \tau_1$ and
$\Gamma, x{:}\tau_2, \Gamma' \vdash e_4 : \tau_4$ (for some $\tau_4$).
We thus use the induction hypothesis on both $e_3$ and $e_4$ to conclude
that $\Gamma, \Gamma' \vdash e_3[x \mapsto e_2] : \tau_4 \to \tau_1$ and
$\Gamma, \Gamma' \vdash e_4[x \mapsto e_2] : \tau_4$. Thus, we can
use \textsc{App} to conclude $\Gamma, \Gamma' \vdash (e_3\,e_4)[x \mapsto e_2] : \tau_1$
as desired.
\item[Case $e_1 = e_3 + e_4$:]
This is similar to the previous case, using \textsc{Plus} instead of \textsc{App}.
\item[Case $e_1 = \lambda x'{:}\tau_3. e_3$:]
The induction hypothesis tells us that if $\Gamma, x{:}\tau_2, \Gamma_2 \vdash e_3 : \tau_4$
(for some $\Gamma_2$ and $\tau_4$), then $\Gamma, \Gamma_2 \vdash e_3[x \mapsto e_2] : \tau_4$.\footnote{Why
so I suddenly change $\Gamma'$ to $\Gamma_2$ here? Note the way the lemma is stated. It first quantifies
universally over $e_1$, and only afterward over all the other variables. This means that the induction
hypothesis is actually universally quantified over all the other variables involved. In the
\textsc{App} case, we don't need the extra flexibility, except for the type. In the $\lambda$
case, though, we do need to be flexible around the context, and so I've written it that way.}
We have assumed that $\Gamma, x{:}\tau_2, \Gamma' \vdash \lambda x'{:}\tau_3.e_3 : \tau_1$.
This can be only by \textsc{Abs}. Thus, we can assume the premise of \textsc{Abs}:
$\Gamma, x{:}\tau_2, \Gamma', x'{:}\tau_3 \vdash e_3 : \tau_4$ (and it must be that
$\tau_1 = \tau_3 \to \tau_4$). That satisfies the premise of the induction hypothesis
(choosing $\Gamma_2 = \Gamma', x'{:}\tau_3$; remember that the induction hypothesis
is universally quantified over this choice of $\Gamma_2$, so this is legal), and so
we conclude $\Gamma, \Gamma', x'{:}\tau_3 \vdash e_3[x \mapsto e_2] : \tau_4$.
By \textsc{Abs}, we can thus conclude $\Gamma, \Gamma' \vdash (\lambda x'{:}\tau_3 . e_3)[x \mapsto e_2] : \tau_3 \to \tau_4$
as desired (remembering that $\tau_1 = \tau_3 \to \tau_4$).
\item[Case $e_1 = n$:] We must prove that $\Gamma, \Gamma' \vdash n[x \mapsto e_2] : \Int$; that is,
that $\Gamma, \Gamma' \vdash n : \Int$. We have this by \textsc{Int}, and we are done.
\end{description}
\end{proof}

\begin{theorem}[Preservation]
\label{thm:preservation}
For all contexts $\Gamma$, expressions $e$ and $e'$, and types $\tau$,
if $\Gamma \vdash e : \tau$ and $e \steps e'$, then $\Gamma \vdash e' : \tau$.
\end{theorem}

\begin{proof}
By structural induction on $e$.
\begin{description}
\item[Case $e = x$:] This case is impossible, because there is no $e'$ such
that $x \steps e'$.
\item[Case $e = e_1\,e_2$:] We have assumed that
$\Gamma \vdash e_1\,e_2 : \tau$. This can be true only by \textsc{App}. Thus,
we know the premises of that rule must be true: $\Gamma \vdash e_1 : \tau' \to \tau$ (for some $\tau'$)
and $\Gamma \vdash e_2 : \tau'$. The induction hypothesis tells us that, if $e_1 \steps e_1'$ (for
some $e_1'$), then $\Gamma \vdash e_1' : \tau' \to \tau$; it also says that if
$e_2 \steps e_2'$ (for some $e_2'$), then $\Gamma \vdash e_2' : \tau'$.

 We have also assumed that $e_1\,e_2 \steps e'$.
This fact can be true in one of three ways; thus we have three cases to consider:
\begin{description}
\item[Case \textsc{App1}:] In this case, we know $e' = e_1'\,e_2$ and $e_1 \steps e_1'$.
By the induction hypothesis, $\Gamma \vdash e_1' : \tau' \to \tau$. We can thus use \textsc{App}
to prove that $\Gamma \vdash e_1' \, e_2 : \tau$ as desired.
\item[Case \textsc{App2}:] In this case, we know $e_1 = v_1$, $e' = v_1 \, e_2'$, and $e_2 \steps e_2'$.
By the induction hypothesis, $\Gamma \vdash e_2' : \tau'$. We can thus use \textsc{App}
to prove that $\Gamma \vdash v_1 \, e_2' : \tau$ as desired.
\item[Case \textsc{Beta}:] In this case, we know $e_1 = \lambda x{:}\tau'.e_3$ and
$e_2 = v$ (for some $e_3$ and $v$). We must prove that $\Gamma \vdash e_3[x \mapsto v] : \tau$.
We showed above that $\Gamma \vdash \lambda x{:}\tau'. e_3 : \tau' \to \tau$. Thus must be
by \textsc{Abs}. Thus, we may conclude the premise to \textsc{Abs}: $\Gamma, x{:}\tau' \vdash e_3 : \tau$.
Now, we use the Substitution Lemma (Lemma~\ref{lem:subst}), instantiating $\Gamma'$ in that lemma statement
to be the empty context. The Substitution Lemma concludes that $\Gamma \vdash e_3[x \mapsto v] : \tau$, as
desired.
\end{description}
\item[Case $e = e_1 + e_2$:] We have assumed that
$\Gamma \vdash e_1 + e_2 : \tau$. This can be true only by \textsc{Plus}. We can thus conclude
that $\tau = \Int$. We can also conclude the premises of \textsc{Plus}:
$\Gamma \vdash e_1 : \Int$ and $\Gamma \vdash e_2 : \Int$. The induction hypothesis tells
us that, if $e_1 \steps e_1'$ (for some $e_1'$) then $\Gamma \vdash e_1' : \Int$, and similarly
for $e_2$.

We have assumed that $e_1 + e_2 \steps e'$. This fact can be true in one of three ways;
thus we have three cases to consider:
\begin{description}
\item[Case \textsc{Plus1}:]
In this case, we know $e' = e_1' + e_2$ and that $e_1 \steps e_1'$. We thus use the
induction hypothesis to get $\Gamma \vdash e_1' : \Int$. We can now use \textsc{Plus}
to conclude $\Gamma \vdash e_1' + e_2 : \Int$ as desired.
\item[Case \textsc{Plus2}:]
In this case, we know $e' = v_1 + e_2'$, $e_1 = v_1$, and $e_2 \steps e_2'$. We thus use
the induction hypothesis to get $\Gamma \vdash e_2' : \Int$. We can now use \textsc{Plus}
to conclude $\Gamma \vdash v_1 + e_2' : \Int$ as desired.
\item[Case \textsc{Add}:]
In this case, we know $e_1 = n_1$, $e_2 = n_2$ and $e' = \textsf{plus}(n_1,n_2)$. Because
$\textsf{plus}(n_1,n_2)$ is a number, we use \textsc{Int} to conclude
$\Gamma \vdash \textsf{plus}(n_1,n_2) : \Int$ as desired.
\end{description}
\item[Case $e = \lambda x{:}\tau'.e_1$:] Impossible, because there exists no $e'$ such
that $e \steps e'$.
\item[Case $e = n$:] Impossible, because there exists no $e'$ such that $e \steps e'$.
\end{description}
\end{proof}

\begin{theorem}[Progress]
\label{thm:progress}
For all expressions $e$ and types $\tau$, if $\emptyset \vdash e : \tau$, then
either $e$ is a value $v$ or there exists $e'$ such that $e \steps e'$.
\end{theorem}

\begin{proof}
By structural induction on $e$.
\begin{description}
\item[Case $e = x$:] We have assumed $\emptyset \vdash x : \tau$, but this
is impossible, because the premise of \textsc{Var} cannot be satisfied in an
empty context.
\item[Case $e = e_1\,e_2$:]
We have assumed that $\emptyset \vdash e_1\,e_2 : \tau$. This can be only
by \textsc{App}. Thus, we can conclude the premises of \textsc{App}, that
$\emptyset \vdash e_1 : \tau' \to \tau$ and $\emptyset \vdash e_2 : \tau'$
for some $\tau'$. The induction hypothesis says that if $e_1$ is well-typed
in an empty context, then it is either a value or it steps to some $e_1'$.
We use this induction hypothesis, giving us two possibilities:
\begin{description}
\item[Case $e_1$ is a value $v_1$:]
We now must use the induction hypothesis on $e_2$ (which we know is well-typed
by the premise of \textsc{App}), giving us two further possibilities:
\begin{description}
\item[Case $e_2$ is a value $v_2$:]
We know $e_1$ is a value, and furthermore that $\emptyset \vdash e_1 : \tau' \to \tau$.
Values have two possibilities: they are either $\lambda$-expressions or integers. If
$v_1$ were an integer, then it could be well-typed only by \textsc{Int}; this would be
a contradiction with the fact that $v_1$'s type is $\tau' \to \tau$. Thus, $v_1$ must
be a $\lambda$-expression. We can conclude then that $v_1 = \lambda x{:}\tau'. e_3$.
Putting this all together, we see that our original expression $e = (\lambda x{:}\tau'. e_3) v_2$.
Thus, \textsc{Beta} applies, and we say that $e' = e_3[x \mapsto v_2]$ and $e \steps e'$.
\item[Case $e_2 \steps e_2'$:] In this case, \textsc{App2} applies. We choose
$e' = v_1 \, e_2'$ and we are done.
\end{description}
\item[Case $e_1 \steps e_1'$:] In this case, \textsc{App1} applies. We choose
$e' = e_1' \, e_2$ and we are done.
\end{description}
\item[Case $e = e_1 + e_2$:]
We have assumed that $\emptyset \vdash e_1 + e_2 : \tau$. This can be only
by \textsc{Plus}, and thus $\tau = \Int$. We can also conclude that
$\emptyset \vdash e_1 : \Int$ and $\emptyset \vdash e_2 : \Int$.
The induction hypothesis says that either $e_1$ is a value or $e_1 \steps e_1'$;
also, either $e_2$ is a value or $e_2 \steps e_2'$. We invoke the induction hypothesis
on $e_1$, giving us two cases:
\begin{description}
\item[Case $e_1$ is a value $v_1$:]
We now invoke the induction hypothesis on $e_2$, giving us two further cases:
\begin{description}
\item[Case $e_2$ is a value $v_2$:]
We have assumed that both $v_1$ and $v_2$ have type $\Int$. There are two forms for
values: either $\lambda$-expressions or integers. All $\lambda$-expressions have a type
mentioning an $\to$; thus these values must be integers $n_1$ and $n_2$. Accordingly,
\textsc{Add} applies, and we choose $e' = plus(n_1,n_2)$ and we are done.
\item[Case $e_2 \steps e_2'$:]
In this case, \textsc{Plus2} applies. We choose $e' = v_1 + e_2'$ and we are done.
\end{description}
\item[Case $e_1 \steps e_1'$:]
In this case, \textsc{Plus1} applies. We choose $e' = e_1' + e_2$ and we are done.
\end{description}
\item[Case $e = v$:] If $e$ is a value, we are trivially done.
\end{description}
\end{proof}

\begin{theorem}[Type safety]
For any natural number $k$: For all expressions $e$ and types $\tau$, if $\emptyset \vdash e : \tau$, then
there exists $e'$ such that $e \steps^k e'$ (that is,
$e$ steps to $e'$ in $k$ steps) and $\emptyset \vdash e' : \tau$,
or there exists $v$ and $k'$ such that $e \steps^{k'} v$ and
$k' < k$.
\end{theorem}

\noindent More intuitively, this means that any well-typed term either steps
forever or eventually reduces to a value.

\begin{proof}
By induction on $k$.
\begin{description}
\item[Case $k = 0$:] Trivially true, as $e \steps^0 e$.
\item[Case $k = j + 1$:] The induction hypothesis gives us either $e'$ such that
$e \steps^j e'$ with $\emptyset \vdash e' : \tau$
 or a value $v$ and $e \steps^{j'} v$ where $j' < j$. We thus have two cases:
\begin{description}
\item[Case $e \steps^j e'$:] We invoke the Progress Theorem (Theorem~\ref{thm:progress}),
giving us two cases:
\begin{description}
\item[Case $e'$ is a value $v$:] We thus have $e \steps^j v$, and $j < k$ (because $k = j + 1$);
we are done.
\item[Case $e' \steps e''$:] In this case, we invoke the Preservation Theorem
(Theorem~\ref{thm:preservation}) to get $\emptyset \vdash e'' : \tau$. We conclude that
$e \steps^k e''$ and are done.
\end{description}
\item[Case $e \steps^{j'} v$ where $j' < j$:] We can observe that $j' < k$ as well,
and we are done.
\end{description}
\end{description}
\end{proof}

\end{document}
