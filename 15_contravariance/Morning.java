public class Morning
{
  public static void main(String[] args)
  {
    Dog d1 = new Dog("Checkers", "Poodle");
    Cat c1 = new Cat("Alice", true);
    Pair<Pet> petPair = new Pair<>(d1, c1);
    Pet max = maxPair(petPair);
    max.speak();
    Dog d2 = new Dog("Clifford", "BigRedDog");
    Dog d3 = new Dog("Sirius", "Animagus");
    Pair<Dog> dogPair = new Pair<>(d2, d3);
    maxPair(dogPair).speak();

    Dog[] dogs = new Dog[3];
    dogs[0] = d1;
    dogs[1] = d2;
    dogs[2] = d3;
    speakArray(dogs);
    dogs[0].speak();

    Cat c2 = new Cat("Esme", false);
    Cat[] cats = new Cat[2];
    cats[0] = c1;
    cats[1] = c2;
    speakArray(cats);
    System.out.println(cats[0].hasClaws());

    boolean b = 3 < 4;
  }

  public static void speakArray(Pet[] pets)
  {
    for(Pet p : pets)
    {
      p.speak();
    }

    pets[0] = new Dog("Pretzel", "Bichon");
  }

  public static <A extends Comparable<? super A>> A maxPair(Pair<A> pair)
  {
    if(pair.getFirst().compareTo(pair.getSecond()) < 0)
    {
      return pair.getSecond();
    }
    else
    {
      return pair.getFirst();
    }
  } 
}
