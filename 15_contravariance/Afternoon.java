public class Afternoon
{
  public static void main(String[] args)
  {
    Dog d1 = new Dog("Clifford", "BigRedDog");
    Cat c1 = new Cat("Tiger", true);
    Pair<Pet> petPair = new Pair<>(d1, c1);
    Pet max = maxPair(petPair);
    max.speak();

    Dog d2 = new Dog("Buttercup", "Collie");
    Pair<Dog> dogPair = new Pair<>(d1, d2);
    Dog maxDog = maxPair(dogPair);
    maxDog.speak();

    Dog[] dogs = new Dog[2];
    dogs[0] = d1;
    dogs[1] = d2;
    speakArray(dogs);
    dogs[0].speak();

    Cat[] cats = new Cat[2];
    cats[0] = c1;
    Cat c2 = new Cat("Minerva", false);
    cats[1] = c2;
    speakArray(cats);
    System.out.println(cats[0].hasClaws());

    boolean b;
    b = 3 > 4;
  }

  public static void speakArray(Pet[] pets)
  {
    for(Pet p : pets)
    {
      p.speak();
    }

    pets[0] = new Dog("Sirius", "Animagus");
  }

  public static <T extends Comparable<? super T>> T maxPair(Pair<T> pair)
  {
    if(pair.getFirst().compareTo(pair.getSecond()) > 0)
    {
      return pair.getFirst();
    }
    else
    {
      return pair.getSecond();
    }
  }
}
