{- Author: Richard Eisenberg
   File: Prime.hs

   Full executable program that detects prime numbers.
-}

module Main where

import Text.Read ( readMaybe )

main :: IO ()
main = do
  putStrLn "Welcome to the prime number checker."
  putStrLn "Enter a number:"
  input <- getLine

  case readMaybe input of
    Just n -> do
      let is_prime = checkPrime n
      if is_prime
        then putStrLn "Yes, it's prime."
        else putStrLn "No, it's not prime."
    Nothing ->
      putStrLn "Invalid entry."

checkPrime :: Int -> Bool
checkPrime n = not (any (\d -> n `mod` d == 0) [2..(n-1)])
