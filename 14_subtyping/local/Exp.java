import java.util.*;

public class Exp
{
  public static void main(String[] args)
  {
    //    int x = 3l;
    long y = 4;
    
    Dog p1 = new Dog("Pretzel", "bichon");
    Dog p2 = new Dog("Sydney", "Havanese");

    p1.speak();
    p2.speak();

    Pair<Dog> p = new Pair<>(p1, p2);
    pairSpeak(p);

    ArrayList<Pair<? extends Pet>> pairList = new ArrayList<>();
    pairList.add(p);

    for(Pair<? extends Pet> pair : pairList)
    {
      pairSpeak(pair);
    }

    Pair<Cat> pairPet = new Pair<>();
    
    putAlice(pairPet);

    pairPet.getSecond().speak();

    Dog d3 = getMin(p);
  }

  public static void pairSpeak(Pair<? extends Pet> pair)
  {
    pair.getFirst().speak();
    pair.getSecond().speak();
  }

  public static void putAlice(Pair<? super Cat> pair)
  {
    pair.setSecond(new Cat("Alice", true));
  }

  public static <T extends Comparable<? super T>> T getMin(Pair<T> p)
  {
    if(p.getFirst().compareTo(p.getSecond()) < 0)
    {
      return p.getFirst();
    }
    else
    {
      return p.getSecond();
    }
  }
}
