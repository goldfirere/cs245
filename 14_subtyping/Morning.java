import java.util.*;

public class Morning
{
  public static void main(String[] args)
  {
    Dog d1 = new Dog("Buddy", "Shih Tzu");

    d1.speak();

    Cat c1 = new Cat("Orange", true);
    c1.speak();

    Pet p1 = d1;
    Pet p2 = c1;

    p1.speak();
    p2.speak();

    ArrayList<Pet> pets = new ArrayList<>();
    pets.add(d1);
    pets.add(c1);

    for(Pet p : pets)
    {
      p.speak();
    }

    Pair<Dog> dogPair = new Pair<>(d1, new Dog("Annie", "Beagle"));
    System.out.println(dogPair.getSecond().getBreed());

    if(dogPair instanceof Pair) // can't check for Pair<Dog>
    {
    }

    // cannot: Pair<Dog>[] pairArray = new Pair<Dog>[5];

    pairSpeak(dogPair);

    Pair<Cat> catPair = new Pair<>(c1, new Cat("Alice", true));
    pairSpeak(catPair);

    ArrayList<Pair<? extends Pet>> pairList = new ArrayList<>();
    pairList.add(dogPair);
    pairList.add(catPair);

    for(Pair<? extends Pet> pair : pairList)
    {
      pairSpeak(pair);
    }

    Pair<Pet> petPair = new Pair<>();
    petPair.setSecond(c1);
    changeFirst(petPair);
    changeFirst(dogPair);
  }

  public static void changeFirst(Pair<? super Dog> pair)
  {
    pair.setFirst(new Dog("Pretzel", "Bichon"));
  }
  
  public static void pairSpeak(Pair<? extends Pet> pair)
  {
    pair.getFirst().speak();
    pair.getSecond().speak();
  }
}
