import java.util.*;

public class Afternoon
{
  public static void main(String[] args)
  {
    Dog d1 = new Dog("Loki", "Boxer hound");
    d1.speak();

    Cat c1 = new Cat("Millie", true);
    c1.speak();

    Pet p1 = d1;
    Pet p2 = c1;

    p1.speak();
    p2.speak();

    // no: Dog d2 = p1;   Pet is not <: Dog

    Pair<Pet> petPair = new Pair<>(d1, c1);

    petPair.getFirst().speak();
    petPair.getSecond().speak();

    // no: if(petPair instanceof Pair<Pet>)
    //    {
    //    }

    // Pair<Dog>[] dogPairs = new Pair<Dog>[6];

    pairSpeak(petPair);

    Pair<Dog> dogPair = new Pair<>(d1, new Dog("Brynn", "Mastiff"));
    pairSpeak(dogPair);

    ArrayList<Pair<? extends Pet>> pairList = new ArrayList<>();
    pairList.add(petPair);
    pairList.add(dogPair);

    for(Pair<? extends Pet> pair : pairList)
    {
      pairSpeak(pair);
    }

    Pair<Cat> catPair = new Pair<>();
    catPair.setSecond(c1);
    
    insertCat(petPair);
    insertCat(catPair);
  }

  public static void insertCat(Pair<? super Cat> pair)
  {
    pair.setFirst(new Cat("Alice", true));
  }
  
  public static void pairSpeak(Pair<? extends Pet> pair)
  {
    pair.getFirst().speak();
    pair.getSecond().speak();
  }
}
