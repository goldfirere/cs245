/* Author: Richard Eisenberg
   File: Lambda.java

   Demonstration of anonymous functions in Java.

   Compare to Lambda.hs, posted on our syllabus page.
*/

import java.util.stream.*;
import java.util.*;

public class Lambda
{
  public static Stream<Character> stringStream(String str)
  {
    return str.chars().mapToObj(c -> (char)c);
    // With thanks to https://stackoverflow.com/questions/22435833/
    //   why-is-string-chars-a-stream-of-ints-in-java-8/22436638#22436638
  }

  // This is more advanced than I expect of students, but it's convenient for
  // writing these examples.
  public static Collector<Character, ?, String> stringCollector
    = Collector.of( StringBuilder::new
		  , StringBuilder::append
		  , StringBuilder::append
		  , StringBuilder::toString);
	
  // Returns the input, but with only the upper-case characters
  public static String uppersOnly(String str)
  {
    return stringStream(str).filter(Character::isUpperCase).collect(stringCollector);
  }

  // Capitalize every letter in the string
  public static String toUpperStr(String str)
  {
    return stringStream(str).map(Character::toUpperCase).collect(stringCollector);
  }

  // increment every number by 1
  public static List<Integer> incByOne(List<Integer> nums)
  {
    return nums.stream().map(x -> x + 1).collect(Collectors.toList());
  }

  // Filter the input list so that only numbers divisible by 2 or 3
  // are in the list
  public static List<Integer> filterNums(List<Integer> nums)
  {
    return nums.stream().filter(x -> x % 2 == 0 || x % 3 == 0)
                        .collect(Collectors.toList());
  }

  // check whether a number is prime
  public static boolean isPrime(int n)
  {
    return n > 1 && IntStream.range(2,n).filter(d -> n % d == 0).count() == 0;
  }

  // all primes up to a limit
  public static List<Integer> allPrimesUpTo(int n)
  {
    return IntStream.range(2,n+1)
                    .filter(Lambda::isPrime)
                    .boxed()
                    .collect(Collectors.toList());
  }

  // all primes. Don't print this out.
  public static Stream<Integer> allPrimes()
  {
    return Stream.iterate(2, x -> x + 1).filter(Lambda::isPrime);
  }
  
  public static void main(String[] args)
  {
    String hello = "Hello There";
    System.out.println(uppersOnly(hello));
    System.out.println(toUpperStr(hello));

    List<Integer> list = Arrays.asList(2,4,6,9,10,11,13,15,17);
    System.out.println(incByOne(list));
    System.out.println(filterNums(list));

    System.out.println(allPrimesUpTo(30));
    System.out.println(allPrimes().limit(30).collect(Collectors.toList()));
  }
}
