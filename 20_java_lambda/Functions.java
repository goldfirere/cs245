/* Author: Richard Eisenberg
   File: Functions.java

   Demonstrates basic functional operations
*/

import java.util.function.*;

public class Functions
{
  // Perform a UnaryOperator (that is, endofunction) twice.
  public static <A> UnaryOperator<A> twice(UnaryOperator<A> op)
  {
    return x -> op.apply(op.apply(x));
  }

  // Compose two functions.
  public static <A,B,C> Function<A,C> compose(Function<B,C> g, Function<A,B> f)
  {
    return x -> g.apply(f.apply(x));
  }
  
  public static void main(String[] args)
  {
    UnaryOperator<Integer> add1 = x -> x + 1;
    UnaryOperator<Integer> add2 = twice(add1);

    System.out.println(add1.apply(5));
    System.out.println(add2.apply(5));

    UnaryOperator<Integer> square = x -> x * x;

    System.out.println(square.apply(5));
    System.out.println(twice(square).apply(5));

    Function<Integer, Integer> addThenSquare = compose(square, add1),
                               squareThenAdd = compose(add1, square);

    System.out.println(addThenSquare.apply(5));
    System.out.println(squareThenAdd.apply(5));
  }
}
