/* Author: Richard Eisenberg
   File: Functions.java

   Demonstrates basic functional operations
*/

import java.util.function.*;

public class Functions
{
  // Perform a UnaryOperator (that is, endofunction) twice.
  public static <A> UnaryOperator<A> twice(UnaryOperator<A> op)
  {
    // return x -> op(op(x));  <-- that is nicer
    return x -> op.apply(op.apply(x));
  }

  // Compose two functions.
  public static <A,B,C> Function<A,C>
    compose(Function<B,C> g, Function<A,B> f)
  {
    return x -> g.apply(f.apply(x));
  }

  // Returns a function that performs f n times.
  public static <A> Function<A,A> nTimes(int n, Function<A,A> f)
  {
    Function<A,A> a = x -> x;
    for(int i = 0; i < n; i++)
    {
      a = compose(a,f);
    }
    return a;
  }

  public static <A> UnaryOperator<A> nTimes(int n, UnaryOperator<A> f)
  {
    if(n == 0)
    {
      return x -> x;
    }
    else
    {
      return x -> nTimes(n-1, f).apply(f.apply(x));
    }
  }
  
  public static void main(String[] args)
  {
    // UnaryOperator<Integer> add1
    //  = (Integer x) -> { return x + 1; };
    //                        \ x -> x + 1
    UnaryOperator<Integer> add1 = x -> x + 1;
    UnaryOperator<Integer> add2 = twice(add1);

    System.out.println(add1.apply(5));
    System.out.println(add2.apply(5));

    UnaryOperator<Integer> square = x -> x * x;

    System.out.println(square.apply(5));
    System.out.println(twice(square).apply(5));

    Function<Integer, Integer>
      addThenSquare = compose(square, add1),
      squareThenAdd = compose(add1, square);

    System.out.println(addThenSquare.apply(5));
    System.out.println(squareThenAdd.apply(5));
  }
}
