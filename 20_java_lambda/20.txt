I. Functional Java
II. Proofs about Types

Functional Java
 - History
 - Functions
 - Streams
 - Lambda

 - Implementation via interfaces
 - Type inference

Exercise:
 - Write nTimes.

Proofs about types: (1:50-2:15)
 1. Every type mentions "Int".
 2. NOT: Every expression has an integer in it.
 3. NOT: Every expression has a lambda or integer in it.
 4. Every *well-typed*, *closed* expression has a lambda or integer in it.
