/* Author: Richard Eisenberg
   File: Memory.java

   Demonstrates different allocation schemes in Java.
*/

public class Memory
{
  // raises base to non-negative power exp
  // arguments are allocated on the stack
  public static int pow(int base, int exp)
  {
    if(exp == 0)
    {
      return 1;
    }
    else
    {
      return base * pow(base, exp-1);
    }
  }

  // increment the first element in an array
  public static void increment(int[] x)
  {
    // x is allocated on the stack, but it refers
    // to an array allocated on the heap
    x[0]++;
  }

  // tries to increment an argument, but doesn't really work
  public static void increment(int x)
  {
    x++;
  }

  public static void main(String[] args)
  {
    System.out.println("2^3: " + pow(2,3));

    // Create a stack variable (num1) that refers to a 1-element
    // array allocated on the heap
    int[] num1 = new int[1];
    num1[0] = 5;

    System.out.println("num1 before increment: " + num1[0]);
    increment(num1);
    System.out.println("num1 after increment: " + num1[0]);

    // Create a stack variable (num2)
    int num2 = 5;
    System.out.println("num2 before increment: " + num2);
    increment(num2);
    System.out.println("num2 after increment: " + num2);

    int[] num3 = num1;
    System.out.println("num3 before increment: " + num3[0]);
    increment(num3);
    System.out.println("num3 after increment: " + num3[0]);

    System.out.println("num1 after incrementing num3: " + num1[0]);
  }
}
