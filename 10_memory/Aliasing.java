/* Author: Richard Eisenberg
   File: Aliasing.java

   Try to predict what this program will print before running it.
*/

public class Aliasing
{
  public static void increment(int[] x)
  {
    x[0]++;
  }

  public static void increment(int x)
  {
    x++;
  }

  public static void increment(int[] x, int[] y)
  {
    x[0]++;
    y[0]++;
  }

  public static void main(String[] args)
  {
    int[] num1 = new int[1];
    num1[0] = 5;
    increment(num1);
    System.out.println(num1[0]);

    int num2 = 5;
    increment(num2);
    System.out.println(num2);

    increment(num1[0]);
    System.out.println(num1[0]);

    int[] num3 = new int[1];
    num3[0] = 5;
    increment(num1, num3);
    System.out.println(num1[0]);
    System.out.println(num3[0]);

    increment(num1, num1);
    System.out.println(num1[0]);

    num3 = num1;
    increment(num3);
    System.out.println(num1[0]);
    System.out.println(num3[0]);

    increment(num1, num3);
    System.out.println(num1[0]);
    System.out.println(num3[0]);
  }
}
