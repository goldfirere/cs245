/* Author: Richard Eisenberg
   File: Overloaded.java
  
   An example showing overloaded methods in Java.
*/

public class Overloaded
{
  public static int method()
  {
    return 10;
  }

  public static int method(int x)
  {
    return x * 2;
  }

  public static int method(int x, int y)
  {
    return x + y;
  }
  
  public static String method(double x)
  {
    return "this is a string: " + x;
  }

  public static void main(String[] args)
  {
    System.out.println(method());
    System.out.println(method(5));
    System.out.println(method(6, 13));
    System.out.println(method(5.));
  }
}
