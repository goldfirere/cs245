/* Author: Richard Eisenberg
   File: sum.h.cc
  
   Adds 3 numbers together.
*/

#include <HERA.h>
#include <HERA-print.h>

void HERA_main()
{
  // Store our 3 numbers where we can find them
  DLABEL(numbers)
    INTEGER(7)
    INTEGER(5)
    INTEGER(9)

  CBON()             // disable addition-carrying
                     // Programs that use values only less than 2^15 will
		     // do this to make ADD easier to work with

  SET(R1, numbers)   // Store the location of "numbers" in R1
  LOAD(R2, 0, R1)    // Store the number 0 places after "numbers" in R2
  LOAD(R3, 1, R1)    // Store the number 1 place after "numbers" in R3
  LOAD(R4, 2, R1)    // Store the number 2 places after "numbers" in R4

  SET(R5, 0)         // Set R5 to 0; R5 will be our sum
  ADD(R5, R2, R5)    // R5 = R2 + R5
  ADD(R5, R3, R5)    // R5 = R3 + R5
  ADD(R5, R4, R5)    // R5 = R4 + R5

  print_reg(R5)      // Print out R5
  HALT()             // End program
  
}
