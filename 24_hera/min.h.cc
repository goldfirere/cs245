/* Author: Richard Eisenberg
   File: min.h.cc

   Finds the minimum of three numbers
*/

#include <HERA.h>
#include <HERA-print.h>

void HERA_main()
{
  // Store our 3 numbers where we can find them
  DLABEL(numbers)
    INTEGER(7)
    INTEGER(5)
    INTEGER(9)

  CBON()             // disable addition-carrying
                     // Programs that use values only less than 2^15 will
		     // do this to make ADD easier to work with

  SET(R1, numbers)   // Store the location of "numbers" in R1
  LOAD(R2, 0, R1)    // Store the number 0 places after "numbers" in R2
  LOAD(R3, 1, R1)    // Store the number 1 place after "numbers" in R3
  LOAD(R4, 2, R1)    // Store the number 2 places after "numbers" in R4

  SUB(R0, R2, R3)    // Compare R2 to R3
  BLR(R2_less)       // If R2 is less, goto R2_less

  SUB(R0, R3, R4)    // Compare R3 to R4
  BLR(R3_smallest)   // If R3 is less, goto R3_smallest
  BRR(R4_smallest)   // goto R4_smallest
  
  LABEL(R2_less)     // This is R2_less
  SUB(R0, R2, R4)    // Compare R2 to R4
  BLR(R2_smallest)   // If R2 is less, goto R2_smallest
  BRR(R4_smallest)   // goto R4_smallest

  LABEL(R2_smallest) // This is R2_smallest
  MOVE(R5, R2)       // Copy R2 into R5
  BRR(done)          // goto done

  LABEL(R3_smallest) // This is R3_smallest
  MOVE(R5, R3)       // Copy R3 into R5
  BRR(done)          // goto done

  LABEL(R4_smallest) // This is R4_smallest
  MOVE(R5, R4)       // Copy R4 into R5
  BRR(done)          // goto done

  LABEL(done)        // This is done
  print_reg(R5)      // Print R5
  HALT()             // Quit.
  
}
