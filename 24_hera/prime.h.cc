/* Author: Richard Eisenberg
   File: Prime.hera

   Checks whether a number is prime.
*/

#include <HERA.h>
#include <HERA-print.h>

void HERA_main()
{
  DLABEL(target_num)
    INTEGER(21)          // Change the number here to test a new number

  CBON()                 // all single-word arithmetic
                         // This command sets the "carry-block", which
                         // means that the processor won't carry from
                         // one addition to another
    
  SET(R2, target_num)    // address of input
                         // This sets R2 to have the address of the label
                         // target_num above.
                         
  LOAD(R1, 0, R2)        // load input into R1
    
  SET(R2, 2)             // load initial divisor into R2
    
  MOVE(R6,R1)            // first arg to mod is always R1 (input)
    
  LABEL(test_loop)
    /* LOOP LOCALS:
     *  R1: number to be tested for primality
     *  R2: current divisor being tested
     */
  SUB(R0,R1,R2)          // compare input to divisor
                         // This tries to subtract divisor from the
                         // number to be tested, but discards the result.
                         // Instead, it just sets the s/z flags
    
  BLER(prime)            // if input <= divisor, number is prime
    
  MOVE(R7,R2)            // second arg to mod is divisor
  CALL(FP_alt, modulus)  // compute modulus
  ADD(R0,R8,R0)          // check if result, R8, is zero
  BZR(not_prime)         // if it is, number is not prime
  INC(R2,1)              // increment test divisor
  BRR(test_loop)         // and try again

  LABEL(not_prime)
  MOVE(R3,R2)            // record test divisor as output R3
  BRR(print_result)

  LABEL(prime)
  SET(R3,0)              // record 0 as output (meaning "prime")
  BRR(print_result)

  LABEL(print_result)
  print_reg(R3)          // print out result
  HALT()                 // don't fall into modulus function

    /* FUNCTION modulus:
     *  Computes the modulus of R6 by R7, placing the result in R8
     *  Uses Rt (R11) for temporary storage
     */
  LABEL(modulus)
    MOVE(Rt,R6)          // Rt is the running difference; Rt = R6
    LABEL(modulus_loop)
    MOVE(R8,Rt)          // copy difference into result; R8 = Rt
    SUB(Rt,R8,R7)        // subtract back into; Rt = R8 - R7
    BGER(modulus_loop)   // if we're > 0, repeat
    RETURN(FP_alt, PC_ret)  // otherwise R8 is the modulus
}
