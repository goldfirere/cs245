/* Author: Richard Eisenberg
   File: triangle.h.cc

   Computes the nth triangular number.
*/

#include <HERA.h>
#include <HERA-print.h>

void HERA_main()
{
  // Store the value 7 in the label "n"
  DLABEL(n)
    INTEGER(7)
  
  CBON()             // disable addition-carrying
                     // Programs that use values only less than 2^15 will
		     // do this to make ADD easier to work with
		     
  SET(R2, n)         // load the address "n" into R2
  LOAD(R1, 0, R2)    // lookup R2 (that is, "n") in memory and load the
                     // information there into R1
  
  SET(R3, 0)         // Set R3 to 0. R3 will be our result.
  SET(R2, 0)         // Set R2 to 0. R2 will be our loop variable.
  
  LABEL(loop_begin)  // This marks the beginning of the loop.
  ADD(R3, R2, R3)    // R3 = R2 + R3
  INC(R2, 1)         // R2 += 1
  SUB(R0, R2, R1)    // compute R2 - R1, but discard the result
                     // (R0 is a special register that is always 0; it cannot
		     // be written to.)
  BLER(loop_begin)   // If the previous arithmetic operation was less-than-or-equal
                     // to 0 (the "LE" means Less-than-or-Equal), then goto loop_begin
		     // In other words, if the loop variable is less than or equal
		     // to "n", keep looping
		     
  print_reg(R3)      // Print the result
  HALT()             // End program
  
}
