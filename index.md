---
title: "CS 245: Principles of Programming Languages"
---

<div id="header">

| **CS 245: Principles of Programming Languages**
| Prof. Richard Eisenberg
| Fall 2018
| Bryn Mawr College

</div>

\$navbar\$

General information
===================

<div id="info_table">

----------------------         -----------------------------------------------------------------------------------------------------------------------------------------
Instructor:                    [Richard Eisenberg](http://cs.brynmawr.edu/~rae)
Email:                         `rae@cs.brynmawr.edu`
Office Phone:                  610-526-5061
Home Phone (emergencies only): 484-344-5924
Cell Phone (emergencies only): 201-575-6474 (no texts, please)
Office:                        Park 204
Office Hours:                  Tuesdays 11:25am-12:25pm; Wednesdays 1:30pm-2:30pm
                               If these don't work, do not fret. Email instead.
<span class="strut" />
Lecture:                       TR 9:55-11:15am in Park 100 / TR 12:55-2:15pm in Park 245
Lecture Recordings:            at Tegrity: access via [Moodle](https://moodle.brynmawr.edu/course/view.php?id=2279); look for link on right side of screen.
Lab:                           W 9:40-11:00am in Park 231 / W 2:40-4:00pm in Park 231
Website:                       <http://cs.brynmawr.edu/cs245>
GitLab Repo:                   <https://gitlab.com/goldfirere/cs245>
Piazza Q&A Forum:              <https://piazza.com/brynmawr/fall2018/cs245/home>
TAs:                           Eileen Feng \<<efeng@brynmawr.edu>\>, My Nguyen \<<mnguyen1@brynmawr.edu>\>, Sehyeok Park \<spark5@haverford.edu\>, Pablo Thiel \<<pthiel@haverford.edu>\>, Mark Xu \<<txu1@haverford.edu>\>
----------------------         -----------------------------------------------------------------------------------------------------------------------------------------

</div>

TA Hours
--------

<div id="ta_hours">
--------------------           ------------------------------     ----------------------
Time                           TA                                 Location
Sundays 7-9pm                  Mark Xu                            Park 231 (BMC)
Sundays 7-9pm                  Pablo Thiel                        Hilles 110 (HC)
Mondays 7-9pm                  My Nguyen                          Park 231 (BMC)
Mondays 7-9pm                  Pablo Thiel                        Hilles 110 (HC)
Tuesdays 7-9pm                 Sehyeok Park                       Park 231 (BMC)
Wednesdays 6-8pm               Eileen Feng                        Park 231 (BMC)
Wednesdays 7-9pm               Sehyeok Park                       Hilles 110 (HC)
Thursdays 6-8pm                Mark Xu                            Hilles 110 (HC)
Thursdays 7-9pm                Eileen Feng \& My Nguyen           Park 231 (BMC)
--------------------           ------------------------------     ----------------------
</div>

Goals of course
---------------

<div id="goals">

By the end of this course, you will be able to...

* describe the utility and salient features of a variety of programming language features
* compare and contrast different programming languages
* read language specifications / documentation
* engage with real-world language evolution
* (ab)use advanced features of Java
* write a language interpreter in the functional programming language Haskell
* explain what *types* are

During the course, you will...

* code in Java and Haskell (and, to a lesser extent, a few other languages)
* read language specifications / documentation
* refine coding style
* work with the command-line / terminal to compile your programs
* debate language features and syntax
* work with other students every class

</div>

This course is an introduction to the study of *programming languages*. All programmers
work in some programming language, be it Python, Java, C, C++, Haskell, or something
even more exotic. Where did these languages come from? Why are they designed the way
they are? How do they evolve? How can we contribute to them? When starting a project,
what language should we use?

We will gain a greater understanding of these issues by treating Java as a semester-long
case study in language design. Java was released in 1996 and has evolved significantly
since then. The two biggest innovations have been *generics* (introduced with Java 5 in
2004) and *anonymous functions* (introduced with Java 8 in 2014). Java now plays host
to several key language concepts, chiefly: object orientation, parametric polymorphism, and
functional programming. Java is also a *typed* language (meaning that the compiler
will warn if you, say, try to use a chunk of text as a number), allowing us to study
type systems. The language also demonstrates several design errors -- mistakes in the
design of the language that, in some cases, require that all Java programs run a bit
slower than they otherwise could. These errors are a fantastic window into the design
process.

Alongside our study of Java, we will cover the basics of the Haskell programming
language. Haskell demonstrates some of the same features as Java (parametric polymorphism
and functional programming) while introducing new ones: pervasive type inference
and stateless programming (often called *purity*). Haskell takes a very different
approach to programming than other languages do, and learning it gives one a different
perspective on programming in general. In addition, Haskell is ideal for *implementing*
a language interpreter. Several assignments will involve modifying a Java interpreter,
written in Haskell. (That is, this is a Haskell program that, when it runs, accepts a
Java program as input.)

We will also spend a little time exploring other languages, including assembly language
([HERA](https://www.haverford.edu/computer-science/resources/hera), a local version of
assembly), JavaScript, and, if there is time, Kotlin.

One key tool we will use in our study is that of programming language *specification*,
where the designers of a language write down, with utmost precision, what the language
means. Working with language specifications is a distinct skill, and one that is valuable
to any computer scientist who might need to learn a new language.

By the end of the course, students will leave with an understanding of and appreciation
for the process (and challenges) of language design. In addition, students will be
able to cogently discuss language features and make informed decisions about language
choice in future projects.

Course Philosophy
-----------------

I believe in *hands-on* learning. This course is thus
*programming-intensive*. You will be expected to spend a significant amount of
time weekly (6-8 hours) outside of class programming to complete the homework
assignments. If you run into a snag, the programming burden may prove to be
even higher. Of course, programming is fun, so the time should fly by.

<img class="textbook" src="images/core-java.jpg" alt="Horstmann text cover"/>
<img class="textbook" src="images/pih.jpg" alt="Hutton text cover"/>
<img class="textbook" src="images/textbook.jpg" alt="Louden/Lambert text cover"/>

Materials
=========

<div id="materials">

Textbooks
---------

The required textbooks for this course are:

* **Programming Languages: Principles and Practice**, 3rd Edition, by Kenneth C. Louden
and Kenneth A. Lambert. Course Technology, Cengage Learning, 2012. Available
at the campus bookstore.

* **Programming in Haskell**, 2nd Edition, by Graham Hutton. Cambridge University
Press, 2016. Available at the campus bookstore.

There will also be recommended readings from the following text:

* **Core Java: Volume I -- Fundamentals**, 10th Edition, by Cay S. Horstmann. Prentice
Hall, 2016. Available at the campus bookstore.

Students new to the command line may find the following book helpful, though no readings
will be assigned from it:

* **The Linux Command Line: A Complete Introduction** by William E. Shotts, Jr. No Starch Press, 2012. Available for [free download](http://site.ebrary.com.proxy.brynmawr.edu/lib/brynmawr/detail.action?docID=10574796) through both
the Bryn Mawr and Haverford libraries.

**Please double-check edition numbers before purchasing! Do not get a different edition!**

Websites
--------

There will be assigned readings from the specifications of the two major languages we
will study:

* [The Java Language Specification](https://docs.oracle.com/javase/specs/jls/se8/html/index.html) for Java SE 8. As of July 2018, Java 10 is the most recent version. However, I expect that many
computers in use by students still have Java 8, as Java 9 was released in August 2017, and
Java 10 in March 2018. We will thus restrict ourselves to Java 8 for this iteration of
the course.

* [The Haskell 2010 Report](https://www.haskell.org/onlinereport/haskell2010/). This is
the latest specification of Haskell.

* [The Glasgow Haskell Compiler User's Guide](https://downloads.haskell.org/~ghc/8.4.3/docs/html/users_guide/), for GHC 8.4.3. The Glasgow Haskell Compiler (GHC) is the major compiler for Haskell, and
it contains many, many enhancements over the Haskell2010 language.

While the links above are about *language*, we will also need *libraries*, the functions
that we use to get work done. Library references are available here:

* [Java 8 API Reference](https://docs.oracle.com/javase/8/docs/api/)

* Haskell's [base](http://hackage.haskell.org/package/base) package, which contains
  most of the definitions that ship with GHC.

* [Stackage](https://www.stackage.org/) contains a powerful type-based search facility
(called [Hoogle](https://github.com/ndmitchell/hoogle)) that searches many libraries.

Software
--------

You will need the following software on your computer:

* The [Java 8 JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

* [Haskell](https://www.haskell.org/downloads). There are numerous ways to get Haskell.
I recommend the [Haskell Platform](https://www.haskell.org/downloads#platform). There is
a vocal community of people who advocate for [Stack](https://www.haskellstack.org) instead.
I find that Stack is oriented more toward real-world project development, whereas the Platform
is smoother sailing for the smaller assignments (usually, single files) in a course.

We will *not* be using a specific IDE (such as Eclipse) in this course. How you get your
programs to compile will be up to you. I recommend using a sturdy editor and searching
for Java and Haskell plugins to do syntax highlighting, etc. Good sturdy editors include
Atom, Sublime Text, and Visual Studio Code. I use trusty old emacs, which is ancient but
generally has the best plugin network.

</div>

