The course materials in this repository are used in teaching
courses at Bryn Mawr College, Bryn Mawr, PA, USA.

You are welcome to use these materials for your own purposes. If
you do so in a public setting (course / talk / etc), I would appreciate
attribution. It would also be great to get in touch, so feel free to
drop an email -- I'd love to know what's been useful to you.
