{- Name: <your name here>
   File: Lab2.hs
   Desc: Lab 2 exercises
-}

module Lab2 where

import Prelude hiding ( null, zip, unzip )

-- This function is part of the normal Haskell Prelude
null :: [a] -> Bool
null [] = True
null _  = False

-- "natural number predecessor"
natPred :: Integer -> Maybe Integer
natPred n
  | n <= 0    = Nothing
  | otherwise = Just (n-1)

incMaybe :: Maybe Integer -> Maybe Integer
incMaybe Nothing  = Nothing
incMaybe (Just n) = Just (n+1)

findChar :: Char -> [Char] -> Maybe Integer
findChar _ [] = Nothing
findChar c (x:xs)
  | c == x    = Just 0
  | otherwise = incMaybe (findChar c xs)

snocView :: [a] -> Maybe ([a], a)
snocView []     = Nothing
snocView (x:xs) = Just (go x xs)
  where
    go y []     = ([], y)
    go y (z:zs) = (y:as, a)
      where
        (as, a) = go z zs

find :: Eq a => a -> [a] -> Maybe Integer
find _ [] = Nothing
find c (x:xs)
  | c == x    = Just 0
  | otherwise = incMaybe (find c xs)
