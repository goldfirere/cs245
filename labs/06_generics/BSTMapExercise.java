/* Author: Richard Eisenberg and <your name here>
   File: BSTMapExercise.java

   An implementation of a binary tree, ripe for conversion into a generic class.
*/

// One node in a binary tree, storing two ints as data.
class BSTNode
{
  public int key;
  public int value;

  public BSTNode left = null;
  public BSTNode right = null;

  public BSTNode(int k, int v)
  {
    key = k;
    value = v;
  }
}

// An implementation of an associative map from ints to ints, stored using
// a binary search tree implementation.
class BSTMap
{
  // The root of the tree.
  BSTNode root = null;

  // Construct an empty tree.
  public BSTMap()
  {
  }

  // Add a new mapping in the map from key to value.
  // Postcondition: the key is mapped to the value.
  public void insert(int key, int value)
  {
    BSTNode newNode = new BSTNode(key, value);
    
    if(root == null)
    {
      root = newNode;
    }
    else
    {
      BSTNode cur = root;

      // INVARIANT: cur != null
      while(true)
      {
	if(key == cur.key)
	{
	  // The newNode becomes garbage in this case.
	  cur.value = value;
	  return;
	}
	else if(key < cur.key)
	{
	  if(cur.left == null)
	  {
	    cur.left = newNode;
	    return;
	  }
	  else
	  {
	    cur = cur.left;
	  }
	}
	else
	{
	  if(cur.right == null)
	  {
	    cur.right = newNode;
	    return;
	  }
	  else
	  {
	    cur = cur.right;
	  }
	}
      }
    }
  }

  // Retrieve a value from the map given a key; returns -1 of the key
  // is not mapped to anything.
  public int lookup(int key)
  {
    BSTNode cur = root;

    while(cur != null)
    {
      if(cur.key == key)
      {
	return cur.value;
      }
      else if(key < cur.key)
      {
	cur = cur.left;
      }
      else
      {
	cur = cur.right;
      }
    }

    return -1; // default value
  }
}

// This class just tests our work, above.
public class BSTMapExercise
{
  public static void main(String[] args)
  {
    BSTMap map = new BSTMap();
    map.insert(100, 3);
    map.insert(120, 5);
    map.insert(-4, 8);
    map.insert(6, 10);

    System.out.println(map.lookup(6));
    System.out.println(map.lookup(120));
    System.out.println(map.lookup(5));
  }
}
