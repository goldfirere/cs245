/* Author: Richard Eisenberg
   File: List.java

   Parametric polymorphic (i.e., generic) lists in Java, in the style of Haskell
*/

import java.util.*;

// Haskell datatypes are best implemented in Java as an abstract class with
// subclasses for each variant.

abstract class ListNode<A>
{
}

class Nil<A> extends ListNode<A>
{
  public Nil()
  {
  }

  // Java uses toString to render data to strings. This is like Haskell's "Show"
  public String toString()
  {
    return "[]";
  }
}

class Cons<A> extends ListNode<A>
{
  // Use immutable structures, like Haskell.
  public final A head;
  public final ListNode<A> tail;
  
  public Cons(A x, ListNode<A> xs)
  {
    head = x;
    tail = xs;
  }

  public String toString()
  {
    return head.toString() + ":(" + tail.toString() + ")";
  }
}

// This class is just to collect all the top-level "functions".
public class List
{
  // Compute the length of a list.
  public static <A> int length(ListNode<A> list)
  {
    // This `if` is like Haskell's pattern-match construct
    if(list instanceof Nil)
    {
      return 0;
    }
    else
    {
      final Cons<A> cons = (Cons<A>)list;
      return 1 + length(cons.tail);
    }
  }

  // Increment every element in the list.
  public static ListNode<Integer> increment(ListNode<Integer> list)
  {
    if(list instanceof Nil)
    {
      return new Nil<>();
    }
    else
    {
      final Cons<Integer> cons = (Cons<Integer>)list;
      return new Cons<>(cons.head + 1, increment(cons.tail));
    }
  }

  // Insertion sort requires the ability to compare elements. Java suppports
  // *bounded polymorphism* to accomplish this. It's like Haskell's class
  // constraint mechanism.
  public static <A extends Comparable<A>> ListNode<A> insert(A x, ListNode<A> xs)
  {
    if(xs instanceof Nil)
    {
      return new Cons<>(x, new Nil<>());
    }
    else
    {
      final Cons<A> cons = (Cons<A>)xs;
      if(x.compareTo(cons.head) < 0)
      {
	return new Cons<>(x, cons);
      }
      else
      {
	return new Cons<>(cons.head, insert(x, cons.tail));
      }
    }
  }
  
  public static <A extends Comparable<A>> ListNode<A> insertionSort(ListNode<A> xs)
  {
    if(xs instanceof Nil)
    {
      return new Nil<>();
    }
    else
    {
      final Cons<A> cons = (Cons<A>)xs;
      return insert(cons.head, insertionSort(cons.tail));
    }
  }

  public static <A> Optional<A> listToMaybe(ListNode<A> list)
  {
    if(list instanceof Nil)
    {
      return Optional.empty();
    }
    else
    {
      final Cons<A> cons = (Cons<A>)list;
      return Optional.of(cons.head);
    }
  }

  // Test our functions.
  public static void main(String[] args)
  {
    ListNode<Integer> nums = new Cons<>(8, new Cons<>(2, new Cons<>(10, new Nil<>())));

    System.out.println(nums);
    System.out.println(length(nums));
    System.out.println(increment(nums));
    System.out.println(insertionSort(nums));
    System.out.println(nums);
  }
}
