{- Name: Richard Eisenberg
   File: HelloWorld.hs
   Desc: Hello, Haskell!
-}

main = do
  putStrLn "Hello, world!"
