{- Name: Richard Eisenberg
   File: Intro.hs
   Desc: A few simple functions in Haskell
-}

-- raise a number to a power
pow base 0 = 1
pow base n = base * pow base (n-1)

-- add elements in a list
add []     = 0
add (x:xs) = x + add xs

-- sort a list of numbers
qsort []     = []     -- an empty list is already sorted
qsort (x:xs) = qsort smaller ++ [x] ++ qsort larger
  where smaller = [a | a <- xs, a <= x]
        larger  = [b | b <- xs, b > x]
