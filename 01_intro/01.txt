I. Introductions
II. The study of programming languages
III. Administrivia
IV. Functional programming
V. Intro to Haskell

PREP WEB PAGES!

Introductions (0:55-1:05)
- who I am
- my role in PL community
- who you are (intermediate students)
- new direction within CS -- previous performance does not predict future results
- names next week!

The study of programming languages (1:05-1:25)
- comparison to English: grammar & structure
- "what does a program mean?"
- "where do programming languages come from?"
  - it all started with machine language
  - then assembly
  - then more interesting ones
- geneologies:
    https://letstalkdata.com/wp-content/uploads/2014/05/prog_lang_dendro_blog.jpg
    https://upload.wikimedia.org/wikipedia/commons/2/25/Genealogical_tree_of_programming_languages.svg
    https://en.wikipedia.org/wiki/Timeline_of_programming_languages
    https://www.levenez.com/lang/lang.pdf
- how do we analyze what a program means?
- Weird behavior of ++
  - update-in-place
  - lvalue vs rvalue
  - that's not a type error

- **Syntax**: how we write/read a program
- **Semantics**: what a program means

- Programming Languages enable **abstraction** (just like natural languages!)

- Language *specifications*
  - JLS
  - Haskell Report

- Language *implementations*
  - interpreter
  - bytecode compiler
  - machine-code compiler

Administrivia (1:25-1:45)
- Registration sheet (5 mins)
- lottery stuff
- labs are required
- textbooks
- grading breakdown
- collaborative style work
- quizzes
- late days
- piazza
- gitlab
- office hours
- general work expectations (good style, intentional coding)

Functional programming & Haskell (1:45-2:15)
- https://www.youtube.com/watch?v=z4jeREy7Pbc
- it's all about processing data, not updating state
- Haskell is
  * functional
  * pure
  * lazy
  * richly typed

- Hello world!
- examples
- recursion
- reduction
- GHCi

- Basic functions: square, multAdd, double
- reduction (referential transparency)


