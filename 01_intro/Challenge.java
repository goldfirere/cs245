/* Name: Richard Eisenberg
 * File: Challenge.jav
 * Desc: Demonstrates weird behavior of Java's ++ operator
 */

public class Challenge
{
  public static void main(String[] args)
  {
    int x = 5;
    System.out.println(x);

    x++;
    System.out.println(x);

    x = x++;
    System.out.println(x);

    x = x++ + x++;
    System.out.println(x);

    x = x++ + (x = x++) + x++;
    System.out.println(x);

    x++ = x++;
  }
}
    
    
    
