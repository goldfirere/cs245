/*
 * This file defines the data pseudo-ops for HERA-C:
 * CONSTANT, DLABEL, INTEGER, DSKIP, and LP_STRING
 * NOTE than LP_STRING is not guaranteed to work for
 *  control characters, but it does (should) observe the
 *  C language string codes starting with "\", e.g. "\n".
 */

// *******************************************************************************
// 
//  WARNING: HEAP range definitions should agree with Hassem definitions
//   in Hassem/HERA.c and in Hassem's Grammar.y and the Hassem script
//   if any code (e.g. library functions) might depend on them.
//
// *******************************************************************************

#if ! defined first_space_for_heap
#define first_space_for_heap 0x4000     /* See warning above */
#endif
#define next_free_space_on_heap first_space_for_heap  /* See warning above */
#if ! defined last_space_for_heap
#define last_space_for_heap 0xffe0      /* See warning above */
#endif
static address &next_free_memory = (address &) memory[memory[next_free_space_on_heap]=(next_free_space_on_heap+1),next_free_space_on_heap];


// if next_free + nwords > MSIZE, there's trouble
#define INC_NEXT_FREE_MEMORY_BY(nwords) \
	(( (int) next_free_memory > (int) MSIZE - ((int) (nwords)))) ?	\
		(exit(127), 0)						\
	:								\
		(next_free_memory += (nwords))


#define CONSTANT(label,val)	const word label = word(val);
#define DLABEL(label)   	LABEL_NAME(label): const int label = next_free_memory;
#define INTEGER(I)      	(SETMEM(next_free_memory, (I)), INC_NEXT_FREE_MEMORY_BY(1));
// just leave some space we expect to fill later
#define DSKIP(N)        	(INC_NEXT_FREE_MEMORY_BY(N));


// To work with Ch. 7 of appel, under "Strings" (p. 167 in 2nd ed of C version)
// These are stored as "length-prefixed strings", as in Pascal.
// Since the escape characters are those from C, however, we call them LP_STRING
#define TIGER_STRING(S)	LP_STRING(S)
#define LP_STRING(S) { \
			  SETMEM(next_free_memory, strlen(S)); INC_NEXT_FREE_MEMORY_BY(1); \
			  for (unsigned int i=0; i<strlen(S); i++) { \
			    SETMEM(next_free_memory, S[i]); INC_NEXT_FREE_MEMORY_BY(1); \
			  } \
			}
