#include <iostream>
#include <iomanip>

#if ! defined HERA_C
#error "HERA-print.h is only available (for debugging) in HERA_C simulator"
#endif

using namespace std;

// this gets "__STRING" macro
#include <sys/cdefs.h>

#define print(s)	HERA_print(s);
#define print_reg(r)	HERA_print_reg(r, __STRING(r));
#define print_flag(f)	HERA_print_flag(f);
#define print_flags()	HERA_print_flags();
#define dump()		HERA_dump();
#define print_mem(skip, print) HERA_print_mem(skip, print, false);
#define print_mem_with_addr(skip, print) HERA_print_mem(skip, print, true);


static void HERA_print(const char *s)
{
    cout << s << endl;
}

static void HERA_print_reg(register_accessor x, const char *name)
{
    cout << " " << name << " = ";
    
    cout <<   "0x" << setfill('0') << setw(4)
	 << setbase(16) << uword(x.get_register());
    cout << " = 0" <<  setbase(8) << uword(x.get_register());
    //  cout << " = 0b"<<  setbase(2) << uword(x.get_register());
    cout << " = "  << setbase(10) << x.get_register();
    if (x.get_register() < 0)
	cout << " = " << uword(x.get_register());
    if (x.get_register() < 128 && x.get_register() >= 0 && isprint((x.get_register())))
	cout << " = '"  << char(x.get_register()) << "'";

    cout << endl;
}

static void HERA_print_flag(int f)
{
    static const char *bool_name[2] = { "FALSE", "TRUE" };

    if (f == 0)
	cout << " S: " << bool_name[flags[flag_s]];
    else if (f == 1)
	cout << " Z: " << bool_name[flags[flag_z]];
    else if (f == 2)
	cout << " V: " << bool_name[flags[flag_v]];
    else if (f == 3)
	cout << " C: " << bool_name[flags[flag_c]];
    else if (f == 4)
	cout << " carry-block: " << bool_name[flags[flag_carry_block]];
    else
	cout << " warning: illegal flag print request\n";
    cout << endl;
}

static void HERA_print_flags()
{
    static const char *bool_name[2] = { "FALSE", "TRUE " };

    cout << "   carry-block: " << bool_name[flags[flag_carry_block]];
    cout << "   C: " << bool_name[flags[flag_c]];
    cout << "   V: " << bool_name[flags[flag_v]];
    cout << "   Z: " << bool_name[flags[flag_z]];
    cout << "   S: " << bool_name[flags[flag_s]];

    cout << endl;
}


static void HERA_dump()
{
    cout << endl << "Registers:" << endl;
    print_reg(r0);
    print_reg(r1);
    print_reg(r2);
    print_reg(r3);
    print_reg(r4);
    print_reg(r5);
    print_reg(r6);
    print_reg(r7);
    print_reg(r8);
    print_reg(r9);
    print_reg(r10);
    print_reg(r11);
    print_reg(r12);
    print_reg(r13);
    print_reg(r14);
    print_reg(r15);
    
    cout << endl << "Flags:" << endl;
    for (int x = 0; x < 4; x++)
	HERA_print_flag(x);
}

#if ! defined HERA_print_mem_addr
#define HERA_print_mem_addr(m)	" " << m << ":"
#endif

static void HERA_print_mem(int skip, int print, bool with_addr)
{
  int lo=skip, hi=skip+print;
  cout << "Memory[" << lo << "..." << hi << "] =";
  for (int m=lo; m<hi;m++) {
    if (with_addr) cout << endl << m << ":";
    cout << " " << mem(m);
  }
  cout << endl;
}
