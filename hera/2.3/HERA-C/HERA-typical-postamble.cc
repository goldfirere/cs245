/*
 * This is a sample of what could go after the HERA data and code statements
 *  to make it work with HERA-C, analogous to the preamble.
 */

// To help debugging simple steps of compilers in CS350, print R1 and R2 if nothing was output (like in CMSC 240)
#if defined HERA_C_DETECT_CALLS_TO_PRINT
if (!HERA_C_DETECTED_A_CALL_TO_PRINT) { print_reg(R1); print_reg(R2); }
#endif

HALT();

#if defined TEST_LOCAL_HLIB && TEST_LOCAL_HLIB==1
#if defined HERA_C_STACK_LIB && HERA_C_STACK_LIB==1
#include "../Hassem/HLIB/Tiger-stdlib-stack.hera"
#else
#include "../Hassem/HLIB/Tiger-stdlib-reg.hera"
#endif
#else
#if defined HERA_C_STACK_LIB && HERA_C_STACK_LIB==1
#include <Tiger-stdlib-stack.hera>
#else
#include <Tiger-stdlib-reg.hera>
#endif
#endif
}
