// IF YOU SEE THIS FILE IN THE DEBUGGER, SEE THE NOTES IN main BELOW!

#ifndef HERA_C
#define HERA_C

#define HERA_VERSION_STRING "2.3.0"
#define HERAC_DATE_STRING   "October 2013"

#include <unistd.h>
#include <stdlib.h>  /* exit */
#include <string.h>

static int Hprint(const char *str)
{
    write(1, str, strlen(str));
    return 0; // needed to let us use Hprint in a comma-expression with registers
}

static int Hprintchar(char c)
{
    char s[2];
    s[0] = c;
    s[1] = 0; // probably not needed at all
    write(1, s, 1);
    return 0; // needed to let us use it in a comma-expression with registers
}

// This is a good place for a breakpoint when debugging
static int Herror(const char *str, bool fatal=false)
{
    write(2, str, strlen(str));
    write(2, "\n", 1);
    if (fatal)
	    exit(1);
    return 0; // needed to let us use it in a comma-expression with registers
}

static void doing_RETURN()	// just useful to set breakpoints
{
  ;  // Do nothing ... just do a breakpoint here to help with single-stepping "RETURN"s
     // If you have set such a breakpoint and arrive here in the debugger,
     //  it means the HERA processor is just starting to do the RETURN instruction,
     //  partially done updating the "position in the program" ("program counter)
     //  but not yet having updated any registers for the return ...
     //  doing a "next line" should get you to the target of the return
}

static void doing_CALL()	// just for symmetry with doing_RETURN ... not really needed, but maybe useful
{
  ;
}

#include <HERA-arch.h>
// relevant part of architecture for debugging:
// view the arrays memory, registers,
// and flags (size 4, order s z v c).

#include <HERA-instr.h>
#include <HERA-pseudo.h>
#include <HERA-data.h>
#include <HERA-opcode.h>

#include <ctype.h>
#include <assert.h>
#include <string.h>

int main( int argc, char ** argv )
{
// IF YOU SEE THIS FILE IN THE DEBUGGER,
// EITHER (a) configure the debugger to stop at HERA_main rather than main,
//   OR   (b) switch to one of your HERA source files and set a breakpoint
//            and then continue the program to get there (in Eclipse, click
//            on the green "Resume" triangle or hit F8; in gdb, type "cont").
//
// TO set the debugger to stop at HERA_main, do the following,
// but remember that this will stop at the first thing done by HERA_main,
// which may well be a data declaration you've #include'd.
//
//    If you're using Eclipse,
//		choose "Debug Configurations..." from the "Run" menu,
//		click on the "Debugger" tab, and
//		then change "main" to "HERA_main"
//		in the "Stop on startup at:" box
//		and apply your changes, close the dialog, stop the program (via the red square) and re-enter Debug
//    or if you're using GDB,
//		TYPE "break HERA_main"
//		AT THE "gdb" PROMPT BEFORE USING "run"
//    (you may also want to set a breakpoint at 'Herror' to see errors,
//	or at "doing_RETURN" to help debug programs with function calls,
//	as the debugger "next line" operation doesn't do RETURNs right)
//

	if ( !(  // IF YOU SEE THIS IN THE DEBUGGER, READ THE INSTRUCTIONS ABOVE
		 (&SP.get_register() == &registers[RCOUNT-1]) &&
		 (&FP.get_register() == &registers[RCOUNT-2]) &&
		 (&Rt.get_register() == &registers[RCOUNT-3])
	      ))
	{
	    Herror("Internal inconsistency in HERA-C architecture.\n");
	    Herror("See the start of main in HERA.h for details.\n");
	    exit(1);
	}

	if ( !(
	       (8*sizeof(word) == BITS_PER_WORD) &&
	       (sizeof(word) == sizeof(uword) && sizeof(word) < sizeof(int)) &&
	       (sizeof(signed char) == 1)  // for SETLO sign extension
	      ))
	{
	    Herror("Architecture mismatch between HERA-C and underlying hardware.\n");
	    Herror("See the start of main in HERA.h for details.\n");
	    exit(2);
	}

	void HERA_main();

	Hprint("\nWelcome to the HERA-C HERA " HERA_VERSION_STRING  " simulator, " HERAC_DATE_STRING " edition.\n");
	Hprint("  Please report bugs to davew@cs.haverford.edu\n");
	Hprint("\nStarting HERA program at HERA_main() ...\n\n");

	HERA_main();

	Hprint("\n\nHERA program is done.\n");

	return 0;
} /* end main() */

#endif
