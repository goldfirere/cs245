/*
 *  Most Pseudo-ops for HERA-C defined here;
 *  Watch out for *SLIGHTLY* different defs of same terms in Hassem too
 */

static void breakpoint_here_for_SETCB() { }  /* Set a breakpoint on this function to stop during SETCB */

#define SET(d,v)	SETLO(d, (v)&0xff) SETHI(d, (v)>>8)
#define MOVE(a,b)	OR(a, b,R0)

#define CMP(a,b)	SETC() SUB(r0,a,b)
#define NEG(d,b)	SETC() SUB(d,r0,b)
#define NOT(d,b)	SET(R12, 0xffff) XOR((d), R12, (b))

#define HALT()		return;
#define NOP()		;

#define SETC()		SETF(0x08)
#define CLRC()		CLRF(0x08)

#define SETCB()		breakpoint_here_for_SETCB(); SETF(0x10)
#define CLCCB()		CLRF(0x18)

#define FLAGS(a)	CLRF(0x08) ADD(r0,r0,(a))

/* data pseudo-ops are in HERA-data.h */
/* label-based branches and calls are in HERA-instr.h */
