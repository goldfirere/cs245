// The ## pre-processor operation fuses the two symbols,
//  so that LABEL_NAME(LABEL)(fred) becomes label_fred.
// This then lets us use label_fred as a jump target,
//  and fred as an integer address (for the data pseudo-ops)

// NOTE that labels are done here along with the CALL and branch pseudo-ops,
//  because real branch instructions are not implemented in the simulator

// Heap management is declared here no that "DLABEL" can use it

// *******************************************************************************
// 
//  WARNING: HEAP range definitions should agree with Hassem definitions
//   in Hassem/HERA.c and in Hassem's Grammar.y and the Hassem script
//   if any code (e.g. library functions) might depend on them.
//
// *******************************************************************************
#define next_free_space_on_heap 0x8000  /* See warning above */
#define last_space_for_heap 0xffe0      /* See warning above */
static address &next_free_memory = (address &) memory[memory[next_free_space_on_heap]=(next_free_space_on_heap+1),next_free_space_on_heap];

static int TEMPORARY_VALUE;
static int TEMPORARY_MULT;   // also used in LSL/LSR
static unsigned int TEMPORARY_UVALUE;



#define LABEL_NAME(label)	label_ ## label

#define DO_SETREG(d, value)    ((d).set_register(value))


/* set a register to a value but don't touch flags */
#define SETREG_none(d, value)  (TEMPORARY_VALUE = (value),          	\
			        DO_SETREG((d),TEMPORARY_VALUE)		\
    			       )

/* set a register to a value, adjust s and z but nothing else */
#define SETREG_sz(d, value)  (TEMPORARY_VALUE = (value),          	\
			      DO_SETREG((d),TEMPORARY_VALUE),		\
			      flags[flag_s]= ((TEMPORARY_VALUE)&(s_maxval+1)),\
			      flags[flag_z]= (word(TEMPORARY_VALUE) == 0)     \
    			     )

/* set a register to a value, adjust all flags */
#define SETREG_f(d, value,uval) (TEMPORARY_UVALUE = (uval),           \
				 SETREG_sz((d), (value)),  /* sets T_V */ \
			         flags[flag_v]= (((TEMPORARY_VALUE) > s_maxval) | ((TEMPORARY_VALUE) < s_minval)),\
			         flags[flag_c]= ((TEMPORARY_UVALUE) > (u_maxval)) \
    			     )

#define REFMEM(addr)	     (mem((address) addr))
#define SETMEM(addr, value)  (REFMEM((address) addr) = (value))

#define PACK_THE_FLAGS       ((flags[flag_carry_block])*16 + (flags[flag_c])*8 + (flags[flag_v])*4 + (flags[flag_z])*2 + (flags[flag_s]))
#define CARRY_FOR_ADD	     (int(flags[flag_c] && (!flags[flag_carry_block])))
#define CARRY_FOR_SUB	     (int(flags[flag_c] ||  (flags[flag_carry_block])))

// #define UNIMPLEMENTED(OP)    ((cerr << OP " not implemented in HERA-C yet" << endl),exit(3));
// #define DEPRECATED(OP)       ((void) Herror(OP), (void) Herror(" no longer provided in HERA-C 2.0\n"),exit(4));

// ************************************************
//
// 2.1  Arithmetic, Shift, and Logical Instructions
//
// ************************************************

#define SETLO(d,v)	SETREG_none((d), ((void)(((v)<-128||(v)>255)?Herror("Illegal constant for SETLO"):0),((signed char) (v))));
#define SETHI(d,v)	SETREG_none((d), ((void)(((v)<-128||(v)>255)?Herror("Illegal constant for SETHI"):0),(((reg(d)) & 0xff) | ((v) << 8))));


#define AND(d,a,b)	SETREG_sz((d), (reg(a)) & (reg(b)));
#define  OR(d,a,b)	SETREG_sz((d), (reg(a)) | (reg(b)));

#define ADD(d,a,b)	SETREG_f((d), reg(a)+reg(b)+CARRY_FOR_ADD, ureg(a)+ureg(b)+CARRY_FOR_ADD);
#define SUB(d,a,b)	SETREG_f((d), reg(a)-reg(b)-(1-CARRY_FOR_SUB), (u_maxval+1)+ureg(a)-ureg(b)-(1-CARRY_FOR_SUB));

#define MULT(d,a,b)	(TEMPORARY_MULT = reg(a)*reg(b), DO_SETREG(TMP,unsigned(TEMPORARY_MULT)>>16), DO_SETREG((d),unsigned(TEMPORARY_MULT)&0xffff), flags[flag_s] = TEMPORARY_MULT<0, flags[flag_z] = (TEMPORARY_MULT == 0), flags[flag_v] = TEMPORARY_MULT != (int) reg(d));

# define XOR(d,a,b)	SETREG_sz((d), reg(a) ^ reg(b));


// NEW -- flags for shifts -- should check these
# define LSL(d,b) 	(TEMPORARY_MULT = reg(b), SETREG_none((d), (uword(reg(b))<<(1)) | (CARRY_FOR_ADD       )), flags[flag_c] = TEMPORARY_MULT >= 0x8000);
# define LSR(d,b) 	(TEMPORARY_MULT = reg(b), SETREG_none((d), (uword(reg(b))>>(1)) | (CARRY_FOR_ADD*0x8000)), flags[flag_c] = TEMPORARY_MULT & 0x0001);
# define LSL8(d,b)	 SETREG_none((d), (uword(reg(b))<<(8)));
# define LSR8(d,b)	 SETREG_none((d), (uword(reg(b))>>(8)));
	
# define ASL(d,b)	 SETREG_none((d), (reg(b)*2 + CARRY_FOR_ADD));
# define ASR(d,b)	 SETREG_none((d), (reg(b)/2));


#define DEF_FLAGS(val)	((flags[flag_carry_block] = ((val) & 16)), \
                         (flags[flag_c]           = ((val) & 8)),  \
                         (flags[flag_v]           = ((val) & 4)),  \
                         (flags[flag_z]           = ((val) & 2)),  \
                         (flags[flag_s] = ((val) & 1)))
#define CHECK_FLAGS(u5) ((((u5)<0)|((u5)>0x1f))?((void)Herror("Illegal flag specification"),((u5)&0x1f)):(u5))
#define SETF(u5)	DEF_FLAGS( ((PACK_THE_FLAGS) | (CHECK_FLAGS(u5))) );
#define CLRF(u5)	DEF_FLAGS( ((PACK_THE_FLAGS) & (~(CHECK_FLAGS(u5)))) );
#define SAVEF(d)	DO_SETREG((d), PACK_THE_FLAGS);
#define RSTRF(a)	DEF_FLAGS(reg(a));


# define INC(d,u)	SETREG_f((d), reg(d)+((u)>32?((void)Herror("Illegal increment"),32):((u)<1?(Herror("Illegal increment"),1):(u))), ureg(d)+((u)>32?32:((u)<1?1:(u))));
# define DEC(d,u)	SETREG_f((d), reg(d)-((u)>32?((void)Herror("Illegal decrement"),32):((u)<1?((void)Herror("Illegal decrement"),1):(u))), ureg(d)-((u)>32?32:((u)<1?1:(u))));



// ***********************
//
// 2.2 Memory Instructions
//
// ***********************

#define LOAD(d,o,a)	SETREG_sz((d), REFMEM(reg(a)+(unsigned(o)&0x1f)));
#define STORE(d,o,a)	SETMEM(reg(a)+(unsigned(o)&0x1f), reg(d));


// ***************************************
//
// 2.3 Control-Flow and Other Instructions
//
// ***************************************



// Branches and Call are done in a strange way in the simulator,
//  since the program isn't really stored in RAM.
// Basically, only the label-based pseudo-ops are done.
// Thanks to Todd Miller HC'01 for dealing with the setjmp/longjmp code
//  in the original (32-bit) version of this system.


#define LABEL(label)    LABEL_NAME(label): ;
// tried to do this in code too, but got jumps crossing initializations

#define BRR(label)	{                    goto LABEL_NAME(label) ; }

#define BLR(label)	{ if( ((flags[flag_s]+flags[flag_v]) == 1) )                  goto LABEL_NAME(label); }
#define BGER(label)	{ if(!((flags[flag_s]+flags[flag_v]) == 1) )                  goto LABEL_NAME(label); }
#define BLER(label)	{ if( ((flags[flag_s]+flags[flag_v]) == 1 || flags[flag_z]) ) goto LABEL_NAME(label); }
#define BGR(label)	{ if(!((flags[flag_s]+flags[flag_v]) == 1 || flags[flag_z]) ) goto LABEL_NAME(label); }
#define BULER(label)	{ if( (!flags[flag_c] || flags[flag_z]) )                      goto LABEL_NAME(label); }
#define BUGR(label)	{ if(!(!flags[flag_c] || flags[flag_z]) )                      goto LABEL_NAME(label); }

#define BZR(label)	{ if( flags[flag_z]) goto LABEL_NAME(label); }
#define BNZR(label)	{ if(!flags[flag_z]) goto LABEL_NAME(label); }
#define BCR(label)	{ if( flags[flag_c]) goto LABEL_NAME(label); }
#define BNCR(label)	{ if(!flags[flag_c]) goto LABEL_NAME(label); }
#define BSR(label)	{ if( flags[flag_s]) goto LABEL_NAME(label); }
#define BNSR(label)	{ if(!flags[flag_s]) goto LABEL_NAME(label); }
#define BVR(label)	{ if( flags[flag_v]) goto LABEL_NAME(label); }
#define BNVR(label)	{ if(!flags[flag_v]) goto LABEL_NAME(label); }


#define BR(label)	{ SETREG_none((Rt),__LINE__); BRR(label); }

#define BL(label)	{ SETREG_none((Rt),__LINE__); BLR(label); }
#define BGE(label)	{ SETREG_none((Rt),__LINE__); BGER(label); }
#define BLE(label)	{ SETREG_none((Rt),__LINE__); BLER(label); }
#define BG(label)	{ SETREG_none((Rt),__LINE__); BGR(label); }
#define BULE(label)	{ SETREG_none((Rt),__LINE__); BULER(label); }
#define BUG(label)	{ SETREG_none((Rt),__LINE__); BUGR(label); }

#define BZ(label)	{ SETREG_none((Rt),__LINE__); BZR(label); }
#define BNZ(label)	{ SETREG_none((Rt),__LINE__); BNZR(label); }
#define BC(label)	{ SETREG_none((Rt),__LINE__); BCR(label); }
#define BNC(label)	{ SETREG_none((Rt),__LINE__); BNCR(label); }
#define BS(label)	{ SETREG_none((Rt),__LINE__); BSR(label); }
#define BNS(label)	{ SETREG_none((Rt),__LINE__); BNSR(label); }
#define BV(label)	{ SETREG_none((Rt),__LINE__); BVR(label); }
#define BNV(label)	{ SETREG_none((Rt),__LINE__); BNVR(label); }


/*
 Added the following to make the stack look more interesting in the debugger.
 It also may help find bugs is the CALL/RETURN instructions
 */


#define CORRUPTED "HERA-C simulator detected corrupted return address on stack frame -- quitting"
#define SUSPICIOUS "HERA-C simulator: WARNING --- suspicious FP/SP values during RETURN --- Did you save and restore Rt properly?"

#if ! defined RANDOM_HERA_OFFSET
#define RANDOM_HERA_OFFSET  17
#endif

#define CALL(o,l)  {	DO_SETREG(TMP,__LINE__); \
			SETMEM(reg(SP),next_free_return+RANDOM_HERA_OFFSET);\
			if (reg(SP)+(o) > next_free_space_on_heap) Herror("WARNING --- STACK OVERFLOWING INTO STANDARD HEAP SPACE\n"); \
			DO_SETREG(TMP, reg(FP));	\
			DO_SETREG( FP, reg(SP));	\
			DO_SETREG( SP, reg(SP) + (o));	\
			next_free_return++;		\
			if (!setjmp((returns[next_free_return-1])))	\
				{ goto LABEL_NAME(l); }			\
			else	{  /****** RETURNED TO HERE ******/	\
				DO_SETREG(SP, reg(FP));			\
				DO_SETREG(FP, reg(TMP));		\
				if (reg(FP) > reg(SP) || reg(FP) < reg(SP)-32) Herror(SUSPICIOUS); \
				} \
		     }

#define RETURN()     {	\
			next_free_return--;				\
			if (REFMEM(reg(FP)) != next_free_return+RANDOM_HERA_OFFSET) {	\
				Herror(CORRUPTED);			\
				exit(2);				\
			}						\
			longjmp((returns[next_free_return]),1);		\
		     }


